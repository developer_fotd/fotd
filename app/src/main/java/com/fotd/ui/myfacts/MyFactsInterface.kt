package com.fotd.ui.myfacts

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.FactDetails

/**
 * Created by Mohit
 */
interface MyFactsInterface {

    interface View : BaseInterface.View {

        fun showMessage(message: Int)

        fun refreshListOfFacts(listOfFact: ArrayList<FactDetails>)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun getFacts(factStatus: Int, isFirstStart: Boolean, totalItemCount: Int,
                     lastVisibleItem: Int)
    }
}