package com.fotd.ui.myfacts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.details.DetailsActivity
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.*
import kotlinx.android.synthetic.main.activity_my_facts.*

/**
 * Created by Mohit
 */
class MyFactsActivity : BaseActivity(), MyFactsInterface.View, View.OnClickListener {
    private lateinit var mPresenter: MyFactsInterface.PresenterImpl
    private lateinit var mAdapter: MyFactsAdapter
    private var mFactStatus: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_facts)
        getIntentData()
        setListeners()
        initRecyclerView()

        mPresenter = MyFactsPresenter(this)
        mPresenter.onAttach()
        mPresenter.getFacts(mFactStatus, true, 0, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }


    private fun getIntentData() {
        mFactStatus = intent.getIntExtra(AppConstants.EXTRA_FACT_STATUS, 0)
    }

    fun setListeners() {
        toolbar_ll_main.setOnClickListener(this)
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        my_facts_rv_facts.setLayoutManager(layoutManager)
        mAdapter = object : MyFactsAdapter(this) {
            override fun onItemClicked(factDetails: FactDetails) {
                if (mFactStatus == FactStatus.APPROVED) {
                    val intent = Intent(this@MyFactsActivity, DetailsActivity::class.java)
                    intent.putExtra(EXTRA_FACT_ID, factDetails.factId)
                    intent.putExtra(EXTRA_FACT_TYPE, MY_FACTS)
                    startActivity(intent)
                }
            }
        }
        my_facts_rv_facts.setAdapter(mAdapter)
        setScrollListener()
    }

    /**
     * set scroll listener for pagination
     */
    private fun setScrollListener() {
        val linearLayoutManager = my_facts_rv_facts.getLayoutManager() as LinearLayoutManager
        my_facts_rv_facts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView,
                                    dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = linearLayoutManager.itemCount
                val lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition()
                mPresenter.getFacts(mFactStatus, false, totalItemCount, lastVisibleItem)
            }
        })
    }

    override fun showMessage(message: Int) {
        my_facts_tv_no_data.visibility = View.VISIBLE
        my_facts_rv_facts.visibility = View.GONE
        my_facts_tv_no_data.text = getString(message)
    }

    override fun refreshListOfFacts(listOfFact: ArrayList<FactDetails>) {
        my_facts_tv_no_data.visibility = View.GONE
        my_facts_rv_facts.visibility = View.VISIBLE
        mAdapter.setData(listOfFact)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.toolbar_ll_main) {
            finish()
        }
    }
}