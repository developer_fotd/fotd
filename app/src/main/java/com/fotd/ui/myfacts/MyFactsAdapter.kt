package com.fotd.ui.myfacts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import kotlinx.android.synthetic.main.item_facts.view.*

/**
 * Created by Mohit
 */
abstract class MyFactsAdapter(context: Context) : RecyclerView.Adapter<MyFactsAdapter.ViewHolder>() {
    var mContext: Context = context
    var mListOfFacts = arrayListOf<FactDetails>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvFact = itemView.item_facts_tv
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_facts, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfFacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val factDetails = mListOfFacts.get(position)
        holder.tvFact.setText(factDetails.fact)

        holder.itemView.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        onItemClicked(mListOfFacts.get(holder.getAdapterPosition()))
                    }
                })
    }

    fun setData(listOfFacts: ArrayList<FactDetails>) {
        mListOfFacts = listOfFacts
        notifyDataSetChanged()
    }

    abstract fun onItemClicked(factDetails: FactDetails)
}
