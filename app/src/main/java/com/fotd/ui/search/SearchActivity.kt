package com.fotd.ui.search

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.details.DetailsActivity
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import com.fotd.util.AppConstants.*
import kotlinx.android.synthetic.main.activity_search.*

/**
 * Created by Mohit
 */
class SearchActivity : BaseActivity(), SearchInterface.View, View.OnClickListener {
    private lateinit var mPresenter: SearchInterface.PresenterImpl
    private lateinit var mAdapter: SearchAdapter
    private var mLastTextEdit: Long = 0
    private val mHandler = Handler()
    private val DELAY_AFTER_TYPING: Long = 1000
    private var mSearchString: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setListeners()
        initRecyclerView()

        mPresenter = SearchPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        toolbar_ll_main.setOnClickListener(this)

        search_facts_et_description.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mHandler.removeCallbacks(inputFinishChecker)
            }

            override fun afterTextChanged(s: Editable) {
                if (s.length > 0) {
                    mLastTextEdit = System.currentTimeMillis()
                    mHandler.postDelayed(inputFinishChecker, DELAY_AFTER_TYPING)
                } else {
                    showMessage()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        search_facts_rv.setLayoutManager(layoutManager)
        mAdapter = object : SearchAdapter(this) {
            override fun onItemClicked(clickedPosition: Int, listOffacts: ArrayList<FactDetails>) {
                val intent = Intent(this@SearchActivity, DetailsActivity::class.java)
                intent.putParcelableArrayListExtra(EXTRA_ALL_FACTS, listOffacts)
                intent.putExtra(EXTRA_CLICKED_POSITION, clickedPosition)
                intent.putExtra(EXTRA_FACT_SEARCH, mSearchString)
                intent.putExtra(EXTRA_FACT_TYPE, SEARCH_FACTS)
                startActivity(intent)
            }
        }
        search_facts_rv.setAdapter(mAdapter)
        setScrollListener()
    }

    /**
     * set scroll listener for pagination
     */
    private fun setScrollListener() {
        val linearLayoutManager = search_facts_rv.getLayoutManager() as LinearLayoutManager
        search_facts_rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView,
                                    dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = linearLayoutManager.itemCount
                val lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition()
                mPresenter.searchForFacts(mSearchString, false, totalItemCount, lastVisibleItem)
            }
        })
    }

    private val inputFinishChecker = Runnable {
        if (System.currentTimeMillis() > mLastTextEdit + DELAY_AFTER_TYPING - 500) {
            mSearchString = search_facts_et_description.text.toString().trim()
            mPresenter.searchForFacts(mSearchString, true, 0, 0)
        }
    }

    override fun showLoading() {
        //super.showLoading()
        search_pw_loading.spin()
        search_pw_loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        //super.hideLoading()
        search_pw_loading.stopSpinning()
        search_pw_loading.visibility = View.GONE
    }

    override fun showMessage() {
        search_facts_tv_no_data.visibility = View.VISIBLE
        search_facts_rv.visibility = View.GONE
    }

    override fun refreshListOfFacts(listOfFact: ArrayList<FactDetails>) {
        search_facts_tv_no_data.visibility = View.GONE
        search_facts_rv.visibility = View.VISIBLE
        mAdapter.setData(listOfFact)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.toolbar_ll_main) {
            finish()
        }
    }
}