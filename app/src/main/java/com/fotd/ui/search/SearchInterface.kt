package com.fotd.ui.search

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.FactDetails

/**
 * Created by Mohit
 */
interface SearchInterface {

    interface View : BaseInterface.View {

        fun showMessage()

        fun refreshListOfFacts(listOfFact: ArrayList<FactDetails>)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun searchForFacts(searchText: String, isFirstStart: Boolean, totalItemCount: Int, lastVisibleItem: Int)
    }

}