package com.fotd.ui.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import kotlinx.android.synthetic.main.item_facts.view.*
import java.util.*

/**
 * Created by Mohit
 */
abstract class SearchAdapter(_context: Context) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    val context: Context = _context
    var mListOfFacts = arrayListOf<FactDetails>()

    fun setData(listOfFacts: ArrayList<FactDetails>) {
        this.mListOfFacts = listOfFacts
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvfact = itemView.item_facts_tv
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_facts, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfFacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val factDetails: FactDetails = mListOfFacts.get(holder.adapterPosition)
        holder.tvfact.setText(factDetails.fact)
        holder.itemView.setOnClickListener { onItemClicked(holder.adapterPosition, mListOfFacts) }
    }

    abstract fun onItemClicked(clickedPosition: Int, listOffacts: ArrayList<FactDetails>)
}