package com.fotd.ui.base

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.indemand.fotd.R
import com.fotd.util.AppConstants
import com.wang.avi.AVLoadingIndicatorView
import kotlinx.android.synthetic.main.dialog_bottom_message.*


open class BaseFragment : Fragment(), BaseInterface.View {
    private lateinit var mContext: Context
    var loadingDialog: Dialog? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun showSnackbar(message: String, type: Int) {
        showSnackBarForError(message, type)
    }

    override fun showSnackbar(message: Int, type: Int) {
        showSnackBarForError(getString(message), type)
    }

    override fun showMessageFromBottom(message: String, isCancelable: Boolean, positiveBtnText: String,
                                       positiveBtnRunnable: Runnable?) {
        showMessageFromBottom(null, message, isCancelable, positiveBtnText,
                null, positiveBtnRunnable, null)
    }

    override fun showMessageFromBottom(title: String, message: String, isCancelable: Boolean, positiveBtnText: String, positiveBtnRunnable: Runnable?) {
        showMessageFromBottom(title, message, isCancelable, positiveBtnText,
                null, positiveBtnRunnable, null)
    }

    override fun showMessageFromBottom(message: String, isCancelable: Boolean, positiveBtnText: String, negativeBtnText: String,
                                       positiveBtnRunnable: Runnable?, negativeBtnRunnable: Runnable) {
        showMessageFromBottom(null, message, isCancelable, positiveBtnText,
                negativeBtnText, positiveBtnRunnable, negativeBtnRunnable)
    }

    override fun showMessageFromBottom(title: String?, message: String, isCancelable: Boolean, positiveBtnText: String,
                                       negativeBtnText: String?, positiveBtnRunnable: Runnable?,
                                       negativeBtnRunnable: Runnable?) {
        val bsDialog = BottomSheetDialog(mContext, R.style.BaseBottomSheetDialogThem)
        bsDialog.setContentView(R.layout.dialog_bottom_message)
        bsDialog.setCancelable(isCancelable)
        bsDialog.setCanceledOnTouchOutside(isCancelable)
        bsDialog.bottom_dialog_tv_message.text = message

        if (isCancelable) {
            bsDialog.bottom_dialog_iv_slide.visibility = View.VISIBLE
        } else {
            bsDialog.bottom_dialog_iv_slide.visibility = View.GONE
        }

        if (title == null || title.isEmpty()) {
            bsDialog.bottom_dialog_tv_title.visibility = View.GONE
        } else {
            bsDialog.bottom_dialog_tv_title.visibility = View.VISIBLE
            bsDialog.bottom_dialog_tv_title.text = title
        }

        if (negativeBtnText == null || negativeBtnText.isEmpty()) {
            bsDialog.bottom_dialog_btn_negative.visibility = View.GONE
        } else {
            bsDialog.bottom_dialog_btn_negative.text = negativeBtnText
            bsDialog.bottom_dialog_btn_negative.setOnClickListener {
                bsDialog.dismiss()
                if (negativeBtnRunnable != null) {
                    negativeBtnRunnable.run()
                }
            }
        }
        bsDialog.bottom_dialog_btn_positive.text = positiveBtnText
        bsDialog.bottom_dialog_btn_positive.setOnClickListener {
            bsDialog.dismiss()
            if (positiveBtnRunnable != null) {
                positiveBtnRunnable.run()
            }
        }
        bsDialog.show()
    }

    override fun showLoading() {
        showLoading(null)
    }

    override fun showLoading(message: String?) {
        if (loadingDialog == null) {
            loadingDialog = Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar)
            loadingDialog!!.setContentView(R.layout.dialog_progress)
            loadingDialog!!.setCancelable(false)
            val tvMessage = loadingDialog!!.findViewById<AppCompatTextView>(R.id.dialog_progress_tv_message)
            val ivLoading = loadingDialog!!.findViewById<AVLoadingIndicatorView>(R.id.dialog_progress_iv_loading)

            if (message != null) {
                tvMessage.visibility = View.VISIBLE
                tvMessage.text = message
            } else {
                tvMessage.visibility = View.GONE
            }
        }

        if (!loadingDialog!!.isShowing) {
            loadingDialog!!.show()
        }
    }

    override fun hideLoading() {
        if (loadingDialog != null && loadingDialog!!.isShowing()) {
            loadingDialog!!.dismiss()
        }
    }

    override fun showRatingPopup() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun showSnackBarForError(message: String, type: Int) {
        activity?.runOnUiThread(Runnable {
            try {
                val snackbar = Snackbar.make(activity?.findViewById(android.R.id.content)!!,
                        message, Snackbar.LENGTH_SHORT) /*.setDuration(2500)*/
                val view = snackbar.view
                val tv = view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                tv.maxLines = 3
                tv.gravity = Gravity.CENTER_HORIZONTAL
                tv.textAlignment = View.TEXT_ALIGNMENT_CENTER
                tv.setTextAppearance(mContext, R.style.Font_Regular)
                tv.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                if (type == AppConstants.SnackBarType.MESSAGE) {
                    view.setBackgroundColor(ContextCompat.getColor(mContext,
                            R.color.snackbar_bg_color_message))
                } else {
                    view.setBackgroundColor(ContextCompat.getColor(mContext,
                            if (type == AppConstants.SnackBarType.SUCCESS) R.color.snackbar_bg_color_success else R.color.snackbar_bg_color_error))
                }
                snackbar.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
}