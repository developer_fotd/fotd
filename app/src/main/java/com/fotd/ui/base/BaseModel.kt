package com.fotd.ui.base

import com.clicklabs.data.network.ApiClient
import com.clicklabs.data.network.ApiInterface
import com.clicklabs.data.network.CommonResponse
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BaseModel : BaseInterface.Model {

    override fun getData(
            url: String,
            mapBody: Map<String, Any>,
            apiListener: BaseInterface.Model.ApiListener
    ) {
        val apiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
        val call = apiInterface.getData(url, mapBody.toMap());
        call.enqueue(object : Callback<CommonResponse> {
            override fun onResponse(
                    call: Call<CommonResponse>,
                    response: Response<CommonResponse>
            ) {
                if (response.body() != null) {
                    val commonResponse = response.body() as CommonResponse
                    if (commonResponse.statusCode == 200) {
                        apiListener.onSuccess(commonResponse)
                    } else {
                        apiListener.onError(commonResponse.message!!)
                    }
                } else if (response.message().isNotEmpty()) {
                    //apiListener.onError((response.body() as CommonResponse).message!!)
                    apiListener.onError(response.message())
                } else {
                    apiListener.onError()
                }
            }

            override fun onFailure(
                    call: Call<CommonResponse>,
                    t: Throwable
            ) {
                apiListener.onError()
            }
        })
    }
    override fun postData(
            url: String,
            mapBody: Map<String, Any>,
            apiListener: BaseInterface.Model.ApiListener
    ) {
        val apiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
        val call = apiInterface.postData(url, mapBody.toMap());
        call.enqueue(object : Callback<CommonResponse> {
            override fun onResponse(
                    call: Call<CommonResponse>,
                    response: Response<CommonResponse>
            ) {
                if (response.body() != null) {
                    val commonResponse = response.body() as CommonResponse
                    if (commonResponse.statusCode == 200) {
                        apiListener.onSuccess(commonResponse)
                    } else {
                        apiListener.onError(commonResponse.message!!)
                    }
                } else if (response.message().isNotEmpty()) {
                    //apiListener.onError((response.body() as CommonResponse).message!!)
                    apiListener.onError(response.message())
                } else {
                    apiListener.onError()
                }
            }

            override fun onFailure(
                    call: Call<CommonResponse>,
                    t: Throwable
            ) {
                apiListener.onError()
            }
        })
    }

    override fun postMultipartData(
            url: String,
            mapBody: Map<String, RequestBody>,
            apiListener: BaseInterface.Model.ApiListener
    ) {
        val apiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
        val call = apiInterface.postMultipartData(url, mapBody);
        call.enqueue(object : Callback<CommonResponse> {
            override fun onResponse(
                    call: Call<CommonResponse>,
                    response: Response<CommonResponse>
            ) {
                if (response.body() != null) {
                    val commonResponse = response.body() as CommonResponse
                    if (commonResponse.statusCode == 200) {
                        apiListener.onSuccess(commonResponse)
                    } else {
                        apiListener.onError(commonResponse.message!!)
                    }
                } else if (response.message().isNotEmpty()) {
                    //apiListener.onError((response.body() as CommonResponse).message!!)
                    apiListener.onError(response.message())
                } else {
                    apiListener.onError()
                }
            }

            override fun onFailure(
                    call: Call<CommonResponse>,
                    t: Throwable
            ) {
                apiListener.onError()
            }
        })
    }
}