package com.fotd.ui.base

import com.clicklabs.data.network.CommonResponse
import okhttp3.RequestBody


interface BaseInterface {

    interface View {
        fun showSnackbar(message: String, type: Int)

        fun showSnackbar(message: Int, type: Int)

        fun showMessageFromBottom(message: String, isCancelable: Boolean,
                                  positiveBtnText: String, positiveBtnRunnable: Runnable?)

        fun showMessageFromBottom(title: String, message: String, isCancelable: Boolean,
                                  positiveBtnText: String, positiveBtnRunnable: Runnable?)

        fun showMessageFromBottom(message: String, isCancelable: Boolean,
                                  positiveBtnText: String, negativeBtnText: String,
                                  positiveBtnRunnable: Runnable?, negativeBtnRunnable: Runnable)

        fun showMessageFromBottom(title: String?, message: String, isCancelable: Boolean,
                                  positiveBtnText: String, negativeBtnText: String?,
                                  positiveBtnRunnable: Runnable?, negativeBtnRunnable: Runnable?)

        fun showLoading()

        fun showLoading(message: String?)

        fun hideLoading()

        fun showRatingPopup()
    }

    interface PresenterImpl {
        fun getString(int: Int): String

        fun onAttach()

        fun onDetach()
    }

    interface Model {

        fun getData(
                url: String,
                mapBody: Map<String, Any>,
                apiListener: BaseInterface.Model.ApiListener
        )

        fun postData(
                url: String,
                mapBody: Map<String, Any>,
                apiListener: BaseInterface.Model.ApiListener
        )

        fun postMultipartData(
                url: String,
                mapBody: Map<String, RequestBody>,
                apiListener: BaseInterface.Model.ApiListener
        )

        interface ApiListener {
            fun onSuccess(response: CommonResponse)

            fun onError(message: String)

            fun onError()
        }
    }
}