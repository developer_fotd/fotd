package com.fotd.ui.base

import com.fotd.MyApplication
import com.fotd.util.CommonUtil


open class BasePresenter : BaseInterface.PresenterImpl {
    open val model: BaseModel = BaseModel()
    var mIsViewAttached = false
    override fun getString(int: Int): String {
        return MyApplication.getAppContext().getString(int)
    }

    override fun onAttach() {
        mIsViewAttached = true
    }

    override fun onDetach() {
        mIsViewAttached = false
    }

    open fun isNetworkConnected(): Boolean {
        return CommonUtil.isNetworkAvailable()
    }


    open fun isViewAttached(): Boolean {
        return mIsViewAttached
    }
}