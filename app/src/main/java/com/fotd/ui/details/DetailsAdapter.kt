package com.fotd.ui.details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.indemand.fotd.R
import com.fotd.data.model.ColorCodes
import com.fotd.data.model.FactDetails
import com.fotd.util.AppConstants.LIKE_STATUS_DISLIKED
import com.fotd.util.AppConstants.LIKE_STATUS_LIKED
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.item_details.view.*
import java.net.URLDecoder

/**
 * Created by Mohit
 */
abstract class DetailsAdapter(context: Context) : RecyclerView.Adapter<DetailsAdapter.ViewHolder>() {
    var mContext: Context = context
    var mListOfFacts = arrayListOf<FactDetails>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llParent = itemView.details_ll_parent
        val tvFact = itemView.details_tv_fact

        val tvUserName = itemView.details_tv_name
        val tvAddedOn = itemView.details_tv_time
        val ivUserImage = itemView.details_iv_user

        val rlLike = itemView.details_rl_like
        val ivLike = itemView.details_iv_like
        val tvLikeCount = itemView.details_tv_like_count

        val rlDislike = itemView.details_rl_dislike
        val ivDislike = itemView.details_iv_dislike
        val tvDislikeCount = itemView.details_tv_dislike_count

        val ivFavourite = itemView.details_iv_add_favourite
        val ivShare = itemView.details_iv_share_fact
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_details, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfFacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val factDetails = mListOfFacts.get(holder.adapterPosition)
        val theme = ColorCodes.getColor(holder.adapterPosition)

        var likeResourceId = theme.likeDefaultResId
        var dislikeResourceId = theme.dislikeDefaultResId
        var favResourceId = theme.favDefaultResId
        if (factDetails.userLikeStatus == LIKE_STATUS_LIKED) {
            likeResourceId = theme.likePressedResId
        } else if (factDetails.userLikeStatus == LIKE_STATUS_DISLIKED) {
            dislikeResourceId = theme.dislikePressedResId
        }

        if (factDetails.userFavStatus == 1) {
            favResourceId = theme.favPressedResId
        }

        holder.tvFact.setText(factDetails.fact)
        holder.tvLikeCount.text = factDetails.likeCount.toString()
        holder.tvDislikeCount.text = factDetails.dislikeCount.toString()
        holder.tvUserName.text = factDetails.addedBy
        holder.tvAddedOn.text = CommonUtil.getLastUpdated(factDetails.addedOn)

        try {
            if (factDetails.userImage != null && !factDetails.userImage.isEmpty()) {
                var imageURL: String = ""
                if (factDetails.userImage.contains("userAvatars")) {
                    imageURL = factDetails.userImage
                } else {
                    imageURL = URLDecoder.decode(factDetails.userImage, "UTF-8")
                }
                Glide.with(mContext).load(imageURL).centerCrop().placeholder(R.drawable.ic_loading)
                        .into(holder.ivUserImage);
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


        holder.llParent.setBackgroundColor(mContext.resources.getColor(theme.bgColor))
        holder.tvFact.setTextColor(mContext.resources.getColor(theme.textColor))
        holder.tvUserName.setTextColor(mContext.resources.getColor(theme.textColor))
        holder.tvAddedOn.setTextColor(mContext.resources.getColor(theme.textColor))
        holder.tvLikeCount.setTextColor(mContext.resources.getColor(theme.textColor))
        holder.tvDislikeCount.setTextColor(mContext.resources.getColor(theme.textColor))

        holder.ivLike.setImageResource(likeResourceId)
        holder.ivDislike.setImageResource(dislikeResourceId)
        holder.ivFavourite.setImageResource(favResourceId)
        holder.ivShare.setImageResource(theme.shareResId)

        holder.rlLike.setOnClickListener { onLikeDislike(holder.adapterPosition, true) }

        holder.rlDislike.setOnClickListener { onLikeDislike(holder.adapterPosition, false) }

        holder.ivFavourite.setOnClickListener { onFavourite(holder.adapterPosition) }
    }

    fun setData(listOfFacts: ArrayList<FactDetails>) {
        mListOfFacts = listOfFacts
        notifyDataSetChanged()
    }

    abstract fun onLikeDislike(position: Int, isLiked: Boolean)

    abstract fun onFavourite(position: Int)
}
