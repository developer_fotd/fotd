package com.fotd.ui.details

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.fotd.ui.base.BaseActivity
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import com.fotd.util.AppConstants.*
import kotlinx.android.synthetic.main.activity_details.*

/**
 * Created by Mohit
 */
class DetailsActivity : BaseActivity(), DetailsInterface.View, View.OnClickListener {
    private lateinit var mPresenter: DetailsInterface.PresenterImpl
    private lateinit var mAdapter: DetailsAdapter

    private var mListOfFacts = arrayListOf<FactDetails>()
    private var mCurrentPosition: Int = 0
    private var mFactType: Int = 0
    private var mFactId: Int = 0

    private var mSearchText: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setListeners()
        initRecyclerView()

        mPresenter = DetailsPresenter(this)
        mPresenter.onAttach()

        getIntentData()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun getIntentData() {
        mFactType = intent.getIntExtra(EXTRA_FACT_TYPE, 0)
        if (mFactType == SEARCH_FACTS) {
            mSearchText = intent.getStringExtra(EXTRA_FACT_SEARCH)!!
        }

        if (mFactType == MY_FACTS) {
            mFactId = intent.getIntExtra(EXTRA_FACT_ID, 0)

            mPresenter.getFactDetails(mFactId)
        } else {
            mListOfFacts = intent.getParcelableArrayListExtra(EXTRA_ALL_FACTS)!!
            mCurrentPosition = intent.getIntExtra(EXTRA_CLICKED_POSITION, 0)

            mPresenter.setListOfFacts(mCurrentPosition, mListOfFacts)
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        details_rv_facts.setLayoutManager(layoutManager)
        mAdapter = object : DetailsAdapter(this) {
            override fun onLikeDislike(position: Int, isLiked: Boolean) {
                mPresenter.likeDislikeFact(position, isLiked)
            }

            override fun onFavourite(position: Int) {
                mPresenter.addToFavorite(position)
            }
        }
        details_rv_facts.adapter = mAdapter
        PagerSnapHelper().attachToRecyclerView(details_rv_facts)
        setScrollListener()
    }

    /**
     * set scroll listener for pagination
     */
    private fun setScrollListener() {
        val linearLayoutManager = details_rv_facts.getLayoutManager() as LinearLayoutManager
        details_rv_facts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView,
                                    dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = linearLayoutManager.itemCount
                val lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition()
                mPresenter.getFacts(mFactType, mSearchText, totalItemCount, lastVisibleItem)
            }
        })
    }

    fun setListeners() {
        details_iv_back.setOnClickListener(this)
    }

    override fun closeView() {
        finish()
    }

    override fun setListToAdapter(currentPosition: Int?, listOfFacts: ArrayList<FactDetails>) {
        mAdapter.setData(listOfFacts)
        currentPosition?.let { details_rv_facts.scrollToPosition(currentPosition) }
    }

    override fun refreshView(position: Int) {
        mAdapter.notifyItemChanged(position)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.details_iv_back -> {
                finish()
            }
        }
    }
}