package com.fotd.ui.details

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.FactDetails

/**
 * Created by Mohit
 */
interface DetailsInterface {

    interface View : BaseInterface.View {

        fun closeView()

        fun setListToAdapter(currentPosition: Int?, listOfFacts: ArrayList<FactDetails>)

        fun refreshView(position: Int)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {

        fun setListOfFacts(currentPosition: Int, listOfFacts: ArrayList<FactDetails>)

        fun getFactDetails(factId: Int)

        fun getFacts(factType: Int, searchString: String, totalItemCount: Int, lastVisibleItem: Int)

        fun likeDislikeFact(position: Int, isLiked: Boolean)

        fun addToFavorite(position: Int)
    }
}