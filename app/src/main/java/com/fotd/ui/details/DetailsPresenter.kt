package com.fotd.ui.details

import com.clicklabs.data.network.APIInventory
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.FactDetails
import com.fotd.data.model.FactResponse
import com.fotd.data.model.LikeAPIResponse
import com.fotd.data.network.APIKeyConstant
import com.fotd.data.network.APIKeyConstant.*
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.*
import com.fotd.util.CommonUtil


/**
 * Created by Mohit
 */
class DetailsPresenter(_view: DetailsInterface.View) : BasePresenter(), DetailsInterface.PresenterImpl {
    private var view: DetailsInterface.View = _view
    private var mListOfFacts = arrayListOf<FactDetails>()
    private var mIsPaginationCalled: Boolean = false
    private var mIsMoreDataAvailable: Boolean = true
    private var mSkip = 0
    val LIMIT_DATA = 10

    override fun setListOfFacts(currentPosition: Int, listOfFacts: ArrayList<FactDetails>) {
        mListOfFacts = listOfFacts;
        view.setListToAdapter(currentPosition, listOfFacts)
    }

    override fun getFactDetails(factId: Int) {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_internet_not_connected, AppConstants.SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_FACT_ID to factId)

            model.getData(APIInventory.GET_FACT_DETAILS, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        val factDetails = response.toResponseModel(FactDetails::class.java)
                        mListOfFacts.add(factDetails)
                        view.setListToAdapter(null, mListOfFacts)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun getFacts(factType: Int, searchString: String, totalItemCount: Int, lastVisibleItem: Int) {

        if (CommonUtil.isNetworkAvailable() && factType != MY_FACTS) {
            if (!mIsPaginationCalled && mIsMoreDataAvailable && totalItemCount == lastVisibleItem + 1) {
                mIsPaginationCalled = true
                mSkip = AppConstants.LIMIT_DATA + mSkip
            } else {
                return
            }

            val mapData = mutableMapOf<String, Any>(
                    API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                    API_LIMIT to LIMIT_DATA,
                    API_SKIP to mSkip)

            if (factType == FAVOURITE_FACTS) {
                mapData.put(API_FACT_TYPE, 0)
                mapData.put(API_USER_FAV_FACTS, 1)
            } else if (factType == SEARCH_FACTS) {
                mapData.put(API_FACT_TYPE, 0)
                mapData.put(API_SEARCH_STRING, searchString)
            } else {
                mapData.put(API_FACT_TYPE, factType)
            }

            model.getData(APIInventory.GET_FACTS, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val factResponse: FactResponse = response.toResponseModel(FactResponse::class.java)
                        if (factResponse.facts != null && factResponse.facts.size > 0 && !mIsPaginationCalled) {
                            mListOfFacts = factResponse.facts
                            view.setListToAdapter(null, mListOfFacts)
                        } else if (factResponse.facts != null && mIsPaginationCalled) {
                            mIsMoreDataAvailable = factResponse.facts.size >= AppConstants.LIMIT_DATA
                            //add all data and notify adapter
                            mListOfFacts.addAll(factResponse.facts)
                            mIsPaginationCalled = false
                            view.setListToAdapter(null, mListOfFacts)
                        }
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun likeDislikeFact(position: Int, isLiked: Boolean) {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {

            var status = 0
            if (mListOfFacts.get(position).userLikeStatus == AppConstants.LIKE_STATUS_NOTHING) {
                if (isLiked) {
                    status = LIKE_STATUS_LIKED
                } else {
                    status = LIKE_STATUS_DISLIKED
                }
            } else if (mListOfFacts.get(position).userLikeStatus == AppConstants.LIKE_STATUS_LIKED) {
                if (isLiked) {
                    status = LIKE_STATUS_NOTHING
                } else {
                    status = LIKE_STATUS_DISLIKED
                }
            } else {
                if (isLiked) {
                    status = LIKE_STATUS_LIKED
                } else {
                    status = LIKE_STATUS_NOTHING
                }
            }

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            APIKeyConstant.API_FACT_ID to mListOfFacts.get(position).factId,
                            APIKeyConstant.API_STATUS to status)

            model.postData(APIInventory.FACT_LIKE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val likeAPIResponse: LikeAPIResponse = response.toResponseModel(LikeAPIResponse::class.java)
                        mListOfFacts.get(position).setLikeDislikeResponse(status, likeAPIResponse.likeCount, likeAPIResponse.dislikeCount)
                        view.refreshView(position)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun addToFavorite(position: Int) {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {

            var status = 0
            if (mListOfFacts.get(position).userFavStatus == 0) {
                status = 1
            }

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            APIKeyConstant.API_FACT_ID to mListOfFacts.get(position).factId,
                            APIKeyConstant.API_STATUS to status)

            model.postData(APIInventory.FACT_FAVOURITE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        mListOfFacts.get(position).setUserFavStatus(status)
                        view.refreshView(position)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}