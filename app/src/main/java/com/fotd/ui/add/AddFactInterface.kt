package com.fotd.ui.add

import com.fotd.ui.base.BaseInterface


interface AddFactInterface {

    interface View : BaseInterface.View {

        fun clearTextAndClose()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun addFact(fact: String)
    }
}