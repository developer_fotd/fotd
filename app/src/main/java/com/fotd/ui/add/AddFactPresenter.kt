package com.fotd.ui.add

import com.clicklabs.data.network.APIInventory.Companion.ADD_FACT
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.network.APIKeyConstant.API_ACCESS_TOKEN
import com.fotd.data.network.APIKeyConstant.API_FACT
import com.fotd.util.AppConstants.SnackBarType


class AddFactPresenter(_view: AddFactInterface.View) : BasePresenter(), AddFactInterface.PresenterImpl {
    private var view: AddFactInterface.View = _view

    override fun addFact(fact: String) {
        if (fact.isEmpty()) {
            view.showSnackbar(R.string.add_fact_err_no_fact, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_FACT to fact)

            model.postData(ADD_FACT, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()

                        val message = (getString(R.string.add_fact_tv_instruction_header) + "\n\n"
                                + getString(R.string.add_fact_tv_instruction_one) + "\n"
                                + getString(R.string.add_fact_tv_instruction_two))
                        view.showMessageFromBottom(getString(R.string.add_fact_success), message, false, getString(R.string.text_okay),
                                Runnable {
                                    view.clearTextAndClose()
                                })
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}