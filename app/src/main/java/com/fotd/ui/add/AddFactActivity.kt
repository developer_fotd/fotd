package com.fotd.ui.add

import android.os.Bundle
import android.view.View
import com.fotd.ui.base.BaseActivity
import com.fotd.util.CommonUtil
import com.indemand.fotd.R
import kotlinx.android.synthetic.main.activity_add_fact.*
import kotlinx.android.synthetic.main.activity_signup.*


class AddFactActivity : BaseActivity(), AddFactInterface.View, View.OnClickListener {
    private lateinit var mPresenter: AddFactInterface.PresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_fact)
        setListeners()

        mPresenter = AddFactPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        add_fact_btn_submit.setOnClickListener(this)
        toolbar_ll_main.setOnClickListener(this)
    }

    override fun clearTextAndClose() {
        add_fact_et_description.text!!.clear()
        setResult(RESULT_OK)
        finish()
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.add_fact_btn_submit -> mPresenter.addFact(
                add_fact_et_description.text.toString().trim()
            )

            R.id.toolbar_ll_main -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                finish()
            }
        }
    }
}