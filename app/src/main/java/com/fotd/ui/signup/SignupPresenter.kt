package com.fotd.ui.signup

import com.clicklabs.data.network.APIInventory.Companion.SIGNUP_USER
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.UserAPIResponse
import com.fotd.data.network.APIKeyConstant
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.SnackBarType
import com.fotd.util.CommonUtil
import java.util.*


class SignupPresenter(_view: SignupInterface.View) : BasePresenter(), SignupInterface.PresenterImpl {
    private var view: SignupInterface.View = _view

    override fun signup(name: String, email: String, password: String) {
        if (name.isEmpty()) {
            view.showSnackbar(R.string.err_no_name, SnackBarType.ERROR)
        } else if (CommonUtil.isInvalidEmail(email)) {
            view.showSnackbar(R.string.err_invalid_email, SnackBarType.ERROR)
        } else if (password.isEmpty()) {
            view.showSnackbar(R.string.err_no_password, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_internet_not_connected, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, SnackBarType.ERROR)
        } else {
            view.showLoading()
            var timezoneInMins: Long = 0
            var timezoneInfo = ""
            try {
                timezoneInfo = TimeZone.getDefault().toString()
                timezoneInMins = TimeZone.getDefault().rawOffset /*+ TimeZone.getDefault().getDSTSavings()*/ / 60000.toLong()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_NAME to name,
                            APIKeyConstant.API_EMAIL to email,
                            APIKeyConstant.API_PASSWORD to password,
                            APIKeyConstant.API_DEVICE_TOKEN to Dependencies.getFCMToken(),
                            APIKeyConstant.API_DEVICE_TYPE to AppConstants.DEVICE_TYPE_ANDROID,
                            APIKeyConstant.API_TIMEZONE to timezoneInMins,
                            APIKeyConstant.API_TIMEZONE_INFO to timezoneInfo,
                            APIKeyConstant.API_DEVICE_NAME to "ANDROID")


            model.postData(SIGNUP_USER, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        val userAPIResponse: UserAPIResponse = response.toResponseModel(UserAPIResponse::class.java)
                        Dependencies.saveAccessToken(userAPIResponse.userDetails.accessToken)
                        Dependencies.saveUserDetails(userAPIResponse.userDetails)

                        val runnable = Runnable {
                            view.showHomeScreen()
                        }
                        view.showMessageFromBottom(getString(R.string.text_success), getString(R.string.register_success),
                                false, getString(R.string.text_okay), runnable)
                    }
                }

                override fun onError(message: String) {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}