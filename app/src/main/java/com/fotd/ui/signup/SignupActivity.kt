package com.fotd.ui.signup

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.fotd.ui.HomeActivity
import com.fotd.ui.base.BaseActivity
import com.indemand.fotd.R
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_signup.*


class SignupActivity : BaseActivity(), SignupInterface.View, View.OnClickListener {
    private lateinit var mPresenter: SignupInterface.PresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        setListeners()

        mPresenter = SignupPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        register_tv_back.setOnClickListener(this)
        register_btn.setOnClickListener(this)
    }

    override fun showHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.register_tv_back -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                finish()
            }
            R.id.register_btn ->
                mPresenter.signup(register_et_user_name.text.toString().trim(),
                        register_et_email.text.toString().trim(), register_et_password.text.toString())
        }
    }
}