package com.fotd.ui.signup

import com.fotd.ui.base.BaseInterface


interface SignupInterface {

    interface View : BaseInterface.View {

        fun showHomeScreen()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun signup(name: String, email: String, password: String)
    }
}