package com.fotd.ui.password

import com.fotd.ui.base.BaseInterface


interface ResetInterface {

    interface View : BaseInterface.View {

        fun showLoginScreen()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun updatePassword(accessToken: String, password: String)
    }
}