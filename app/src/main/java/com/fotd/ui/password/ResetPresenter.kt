package com.fotd.ui.password

import com.clicklabs.data.network.APIInventory
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.network.APIKeyConstant.API_ACCESS_TOKEN
import com.fotd.data.network.APIKeyConstant.API_PASSWORD
import com.fotd.util.AppConstants


class ResetPresenter(_view: ResetInterface.View) : BasePresenter(), ResetInterface.PresenterImpl {
    private var view: ResetInterface.View = _view

    override fun updatePassword(accessToken: String, password: String) {
        if (password.isEmpty()) {
            view.showSnackbar(R.string.err_no_password, AppConstants.SnackBarType.ERROR)
        } else if (password.length < AppConstants.PASS_MIN_LENGTH) {
            view.showSnackbar(R.string.err_invalid_password, AppConstants.SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to accessToken,
                            API_PASSWORD to password)

            model.postData(APIInventory.EDIT_PROFILE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showLoginScreen()
                    }
                }

                override fun onError(message: String) {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}