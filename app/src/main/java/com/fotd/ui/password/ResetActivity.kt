package com.fotd.ui.password

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.login.LoginActivity
import com.indemand.fotd.R
import com.fotd.util.AppConstants.EXTRA_ACCESS_TOKEN
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_reset_pass.*

class ResetActivity : BaseActivity(), ResetInterface.View, View.OnClickListener {
    private lateinit var mPresenter: ResetInterface.PresenterImpl
    private lateinit var mAccessToken: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_pass)
        setListeners()
        getIntentData()

        mPresenter = ResetPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    private fun setListeners() {
        reset_tv_back.setOnClickListener(this)
        reset_btn.setOnClickListener(this)
    }

    private fun getIntentData() {
        mAccessToken = intent.getStringExtra(EXTRA_ACCESS_TOKEN)!!
    }

    override fun showLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.reset_btn -> {
                CommonUtil.hideKeyboard(this, reset_et_pass)
                mPresenter.updatePassword(mAccessToken, reset_et_pass.text.toString().trim())
            }

            R.id.reset_tv_back -> {
                CommonUtil.hideKeyboard(this, reset_et_pass)
                finish()
            }
        }
    }

}