package com.fotd.ui.myfacts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.details.DetailsActivity
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import com.fotd.util.AppConstants.*
import kotlinx.android.synthetic.main.activity_favourite.*
import kotlinx.android.synthetic.main.activity_my_facts.toolbar_ll_main

/**
 * Created by Mohit
 */
class FavouritesActivity : BaseActivity(), FavouritesInterface.View, View.OnClickListener {
    private lateinit var mPresenter: FavouritesInterface.PresenterImpl
    private lateinit var mAdapter: FavouritesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        setListeners()
        initRecyclerView()

        mPresenter = FavouritesPresenter(this)
        mPresenter.onAttach()
        mPresenter.getFacts(true, 0, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        toolbar_ll_main.setOnClickListener(this)
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        favourites_rv_facts.setLayoutManager(layoutManager)
        mAdapter = object : FavouritesAdapter(this) {
            override fun onItemClicked(clickedPosition: Int, listOffacts: ArrayList<FactDetails>) {
                val intent = Intent(this@FavouritesActivity, DetailsActivity::class.java)
                intent.putParcelableArrayListExtra(EXTRA_ALL_FACTS, listOffacts)
                intent.putExtra(EXTRA_CLICKED_POSITION, clickedPosition)
                intent.putExtra(EXTRA_FACT_TYPE, FAVOURITE_FACTS)
                startActivity(intent)
            }

            override fun removeFromFavourite(factId: Int) {
                mPresenter.removeFavorite(factId)
            }

            override fun getSwipeLayoutResourceId(position: Int): Int {
                return 0
            }
        }
        favourites_rv_facts.setAdapter(mAdapter)
        setScrollListener()
    }

    /**
     * set scroll listener for pagination
     */
    private fun setScrollListener() {
        val linearLayoutManager = favourites_rv_facts.getLayoutManager() as LinearLayoutManager
        favourites_rv_facts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView,
                                    dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = linearLayoutManager.itemCount
                val lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition()
                mPresenter.getFacts(false, totalItemCount, lastVisibleItem)
            }
        })
    }

    override fun showMessage() {
        favourites_tv_no_data.visibility = View.VISIBLE
        favourites_rv_facts.visibility = View.GONE
    }

    override fun refreshListOfFacts(listOfFact: ArrayList<FactDetails>) {
        favourites_tv_no_data.visibility = View.GONE
        favourites_rv_facts.visibility = View.VISIBLE
        mAdapter.setData(listOfFact)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.toolbar_ll_main) {
            finish()
        }
    }
}