package com.fotd.ui.myfacts

import com.clicklabs.data.network.APIInventory
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.FactDetails
import com.fotd.data.model.FactResponse
import com.fotd.data.network.APIKeyConstant.*
import com.fotd.util.AppConstants
import com.fotd.util.CommonUtil

/**
 * Created by Mohit
 */
class FavouritesPresenter(_view: FavouritesInterface.View) : BasePresenter(), FavouritesInterface.PresenterImpl {
    private var view: FavouritesInterface.View = _view

    private var mIsPaginationCalled: Boolean = false
    private var mIsMoreDataAvailable: Boolean = true
    private var mSkip = 0
    val LIMIT_DATA = 10
    private var mFactDetailsList = arrayListOf<FactDetails>()

    override fun getFacts(isFirstStart: Boolean, totalItemCount: Int, lastVisibleItem: Int) {
        if (CommonUtil.isNetworkAvailable()) {
            if (!isFirstStart) {
                if (!mIsPaginationCalled && mIsMoreDataAvailable && totalItemCount == lastVisibleItem + 1) {
                    mIsPaginationCalled = true
                    mSkip = AppConstants.LIMIT_DATA + mSkip
                } else {
                    return
                }
            }

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_FACT_TYPE to 0,
                            API_USER_FAV_FACTS to 1,
                            API_LIMIT to LIMIT_DATA,
                            API_SKIP to mSkip)

            model.getData(APIInventory.GET_FACTS, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val factResponse: FactResponse = response.toResponseModel(FactResponse::class.java)
                        if (factResponse.facts != null && factResponse.facts.size > 0 && !mIsPaginationCalled) {
                            mFactDetailsList = factResponse.facts
                            view.refreshListOfFacts(mFactDetailsList)
                        } else if (factResponse.facts != null && mIsPaginationCalled) {
                            mIsMoreDataAvailable = factResponse.facts.size >= AppConstants.LIMIT_DATA
                            //add all data and notify adapter
                            mFactDetailsList.addAll(factResponse.facts)
                            mIsPaginationCalled = false
                            view.refreshListOfFacts(mFactDetailsList)
                        } else {
                            view.showMessage()
                        }
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun removeFavorite(factId: Int) {
        if (CommonUtil.isNetworkAvailable()) {
            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_FACT_ID to factId,
                            API_STATUS to 0)

            model.postData(APIInventory.FACT_FAVOURITE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        //view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        //view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}