package com.fotd.ui.myfacts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import kotlinx.android.synthetic.main.item_favourites.view.*

/**
 * Created by Mohit
 */
abstract class FavouritesAdapter(context: Context) : RecyclerSwipeAdapter<FavouritesAdapter.ViewHolder>() {
    var mContext: Context = context
    var mListOfFacts = arrayListOf<FactDetails>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvFact = itemView.item_favourite_tv
        val rlFact = itemView.item_favourite_rl
        val ivDelete = itemView.item_favourite_iv_delete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_favourites, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfFacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val factDetails = mListOfFacts.get(holder.adapterPosition)
        holder.tvFact.setText(factDetails.fact)

        holder.rlFact.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        onItemClicked(holder.adapterPosition, mListOfFacts)
                    }
                })

        holder.ivDelete.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        removeFromFavourite(mListOfFacts.get(holder.adapterPosition).factId);
                        mListOfFacts.removeAt(holder.adapterPosition);
                        notifyItemRemoved(holder.adapterPosition);
                        notifyItemRangeChanged(holder.adapterPosition, mListOfFacts.size);
                    }
                })
    }

    fun setData(listOfFacts: ArrayList<FactDetails>) {
        mListOfFacts = listOfFacts
        notifyDataSetChanged()
    }

    abstract fun onItemClicked(clickedPosition: Int, listOffacts: ArrayList<FactDetails>)

    abstract fun removeFromFavourite(factId: Int)
}
