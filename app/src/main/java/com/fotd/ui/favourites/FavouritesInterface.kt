package com.fotd.ui.myfacts

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.FactDetails

/**
 * Created by Mohit
 */
interface FavouritesInterface {

    interface View : BaseInterface.View {

        fun showMessage()

        fun refreshListOfFacts(listOfFact: ArrayList<FactDetails>)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun getFacts(isFirstStart: Boolean, totalItemCount: Int, lastVisibleItem: Int)

        fun removeFavorite(factId: Int)
    }
}