package com.fotd.ui.about

import android.os.Bundle
import android.view.View
import android.webkit.WebViewClient
import com.fotd.ui.base.BaseActivity
import com.indemand.fotd.R
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_about.*

/**
 * Created by Mohit
 */
class AboutActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        setListeners()
        setData()
    }

    fun setListeners() {
        toolbar_ll_main.setOnClickListener(this)
    }

    fun setData() {
        about_wv_details.setWebViewClient(WebViewClient())
        if (CommonUtil.isNetworkAvailable(this)) {
            about_wv_details.loadUrl(CommonUtil.getAboutUsPage())
        }
    }


    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.toolbar_ll_main -> finish()
            else -> {
            }
        }
    }
}