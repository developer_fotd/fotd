package com.fotd.ui.login

import com.fotd.ui.base.BaseInterface


interface LoginInterface {

    interface View : BaseInterface.View {

        fun showHomeScreen()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun loginUser(email: String, password: String)
    }
}