package com.fotd.ui.login

import com.clicklabs.data.network.APIInventory.Companion.LOGIN_USER
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.UserAPIResponse
import com.fotd.data.network.APIKeyConstant
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.SnackBarType
import com.fotd.util.CommonUtil


class LoginPresenter(_view: LoginInterface.View) : BasePresenter(), LoginInterface.PresenterImpl {
    private var view: LoginInterface.View = _view

    override fun loginUser(email: String, password: String) {
        if (CommonUtil.isInvalidEmail(email)) {
            view.showSnackbar(R.string.err_invalid_email, SnackBarType.ERROR)
        } else if (password.isEmpty()) {
            view.showSnackbar(R.string.err_no_password, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_EMAIL to email,
                            APIKeyConstant.API_PASSWORD to password,
                            APIKeyConstant.API_DEVICE_TOKEN to Dependencies.getFCMToken(),
                            APIKeyConstant.API_DEVICE_TYPE to AppConstants.DEVICE_TYPE_ANDROID,
                            APIKeyConstant.API_DEVICE_NAME to "ANDROID")


            model.postData(LOGIN_USER, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        val userAPIResponse: UserAPIResponse = response.toResponseModel(UserAPIResponse::class.java)
                        Dependencies.saveAccessToken(userAPIResponse.userDetails.accessToken)
                        Dependencies.saveUserDetails(userAPIResponse.userDetails)
                        view.showHomeScreen()
                    }
                }

                override fun onError(message: String) {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}