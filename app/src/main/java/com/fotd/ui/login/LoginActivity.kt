package com.fotd.ui.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.fotd.ui.HomeActivity
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.email.EmailActivity
import com.fotd.ui.signup.SignupActivity
import com.indemand.fotd.R
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity() : BaseActivity(), LoginInterface.View, View.OnClickListener {
     lateinit var mPresenter: LoginInterface.PresenterImpl
    private var doubleBackToExitPressedOnce = false
    private val BACK_PRESS_TIME_IN_MS = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setListeners()

        mPresenter = LoginPresenter(this)
        mPresenter.onAttach()
    }

    fun setListeners() {
        signin_tv_forgot_pass.setOnClickListener(this)
        signin_tv_register.setOnClickListener(this)
        signin_btn.setOnClickListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    override fun showHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.signin_tv_forgot_pass -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(this, EmailActivity::class.java))
            }
            R.id.signin_tv_register -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(this, SignupActivity::class.java))
            }
            R.id.signin_btn -> mPresenter.loginUser(signin_et_email.text.toString().trim(), signin_et_password.text.toString().trim())
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true)
            return
        }
        this.doubleBackToExitPressedOnce = true
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, BACK_PRESS_TIME_IN_MS.toLong())
    }
}