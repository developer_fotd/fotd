package com.fotd.ui.fragments.home

import com.fotd.ui.base.BaseInterface

/**
 * Created by Mohit
 */
interface HomeInterface {

    interface View : BaseInterface.View {
        fun setData(userName: String, fact: String, likeResourceId: Int, likeCount: String, dislikeResourceId: Int,
                    dislikeCount: String, favouriteResourceId: Int)

        fun shareFact(fact: String)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun getHomeFact()

        fun shareFact()

        fun likeFact(isLiked: Boolean)

        fun addToFavorite()
    }
}