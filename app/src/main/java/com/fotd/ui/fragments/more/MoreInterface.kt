package com.fotd.ui.fragments.more

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.AvatarObj
import java.io.File

/**
 * Created by Mohit
 */
interface MoreInterface {

    interface View : BaseInterface.View {

        fun setData(userName: String, userEmail: String, approvedCount: Int, pendingCount: Int,
                    rejectedCount: Int, userImage: String?, notificationText: String)

        fun showLoginScreen()

        fun dismissPopup()

        fun updateProfilePic(imageURL: String)

        fun toggleNotificationText(notificationText: String)

        fun showEditImagePopup(listOfAvatars: ArrayList<AvatarObj>)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {

        fun getLatestData()

        fun getListOfAvatars()

        fun updateUserImage(userImage: File)

        fun checkForAvatar()

        fun toggleForNotifications()

        fun logout()
    }
}