package com.fotd.ui.fragments.blog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fotd.ui.fragments.blog.BlogAdapter.ViewHolder
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import com.fotd.util.AppConstants.*
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.item_blog.view.*
import java.net.URLDecoder

/**
 * Created by Mohit
 */
abstract class BlogAdapter(context: Context) : RecyclerView.Adapter<ViewHolder>() {
    var mContext: Context = context
    var mListOfFacts = arrayListOf<FactDetails>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llParent = itemView.item_blog_ll_parent
        val tvAddedBy = itemView.item_blog_tv_name
        val tvAddedOn = itemView.item_blog_tv_added
        val ivAddedBy = itemView.item_blog_iv_user
        val tvFact = itemView.item_blog_tv_fact
        val btnDetailsWhite = itemView.item_blog_btn_white
        val btnDetailsBlack = itemView.item_blog_btn_black
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_blog, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfFacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val factDetails = mListOfFacts.get(position)
        val drawable = mContext.resources.getDrawable(R.drawable.bg_featured_fact)
        val wrappedDrawable = DrawableCompat.wrap(drawable)

        if (COLOR_RED.contains(position)) {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_0))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.btnDetailsBlack.setVisibility(View.GONE)
            holder.btnDetailsWhite.setVisibility(View.VISIBLE)
        } else if (COLOR_L_BLUE.contains(position)) {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_1))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.btnDetailsBlack.setVisibility(View.VISIBLE)
            holder.btnDetailsWhite.setVisibility(View.GONE)
        } else if (COLOR_D_GREEN.contains(position)) {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_2))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.btnDetailsBlack.setVisibility(View.GONE)
            holder.btnDetailsWhite.setVisibility(View.VISIBLE)
        } else if (COLOR_D_BLUE.contains(position)) {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_3))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.btnDetailsBlack.setVisibility(View.GONE)
            holder.btnDetailsWhite.setVisibility(View.VISIBLE)
        } else if (COLOR_YELLOW.contains(position)) {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_4))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.btnDetailsBlack.setVisibility(View.VISIBLE)
            holder.btnDetailsWhite.setVisibility(View.GONE)
        } else if (COLOR_L_GREEN.contains(position)) {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_5))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.black_default))
            holder.btnDetailsBlack.setVisibility(View.VISIBLE)
            holder.btnDetailsWhite.setVisibility(View.GONE)
        } else {
            DrawableCompat.setTint(wrappedDrawable, mContext.resources.getColor(R.color.fact_bg_0))
            holder.tvFact.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedBy.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.tvAddedOn.setTextColor(mContext.resources.getColor(R.color.white_default))
            holder.btnDetailsBlack.setVisibility(View.GONE)
            holder.btnDetailsWhite.setVisibility(View.VISIBLE)
        }

        holder.llParent.setBackground(drawable)

        holder.tvFact.setText(factDetails.fact)
        holder.tvAddedBy.setText(factDetails.addedBy)
        holder.tvAddedOn.setText(CommonUtil.getLastUpdated(factDetails.addedOn))
        try {
            if (factDetails.userImage != null && !factDetails.userImage.isEmpty()) {
                var imageURL: String = ""
                if (factDetails.userImage.contains("userAvatars")) {
                    imageURL = factDetails.userImage
                } else {
                    imageURL = URLDecoder.decode(factDetails.userImage, "UTF-8")
                }
                Glide.with(mContext)
                        .load(imageURL)
                        .centerCrop()
                        .placeholder(R.drawable.ic_loading)
                        .into(holder.ivAddedBy)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        holder.btnDetailsWhite.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        onItemClicked(holder.getAdapterPosition())
                    }
                })

        holder.btnDetailsBlack.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        onItemClicked(holder.getAdapterPosition())
                    }
                })
    }

    fun setData(listOfFacts: ArrayList<FactDetails>) {
        mListOfFacts = listOfFacts
        notifyDataSetChanged()
    }

    abstract fun onItemClicked(clickedPosition: Int)
}
