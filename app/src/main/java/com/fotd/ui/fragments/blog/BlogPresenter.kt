package com.fotd.ui.fragments.blog

import com.clicklabs.data.network.APIInventory.Companion.GET_BLOG_DETAILS
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.HomeResponse
import com.fotd.data.network.APIKeyConstant
import com.fotd.util.AppConstants.SnackBarType


class BlogPresenter(_view: BlogInterface.View) : BasePresenter(), BlogInterface.PresenterImpl {
    private var view: BlogInterface.View = _view

    override fun getBlogDetails() {
        if (!isNetworkConnected()) {
            view.showMessageFromBottom(getString(R.string.text_oh_dear), getString(R.string.error_internet_not_connected), false,
                    getString(R.string.text_retry),
                    Runnable {
                        getBlogDetails()
                    })
        } else {
            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken())

            model.getData(GET_BLOG_DETAILS, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        //view.hideLoading()
                        val blogResponse = response.toResponseModel(HomeResponse::class.java)
                        view.showBlogDetails(blogResponse)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        //view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        //view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}