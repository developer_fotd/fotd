package com.fotd.ui.fragments.blog

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fotd.ui.base.BaseFragment
import com.fotd.ui.details.DetailsActivity
import com.indemand.fotd.R
import com.fotd.data.model.FactDetails
import com.fotd.data.model.HomeResponse
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.BLOG_FACTS
import kotlinx.android.synthetic.main.fragment_blog.*


class BlogFragment : BaseFragment(), BlogInterface.View {
    private lateinit var mPresenter: BlogInterface.PresenterImpl
    private lateinit var mContext: Context
    private lateinit var mBlogAdapter: BlogAdapter
    private lateinit var mBlogFacts: ArrayList<FactDetails>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = BlogPresenter(this)
        mPresenter.onAttach()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_blog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBlogAdapter = object : BlogAdapter(mContext) {
            override fun onItemClicked(clickedPosition: Int) {
                val intent = Intent(mContext, DetailsActivity::class.java)
                intent.putParcelableArrayListExtra(AppConstants.EXTRA_ALL_FACTS, mBlogFacts)
                intent.putExtra(AppConstants.EXTRA_CLICKED_POSITION, clickedPosition)
                intent.putExtra(AppConstants.EXTRA_FACT_TYPE, BLOG_FACTS)
                startActivity(intent)
            }
        }

        blog_rv_facts.apply {
            layoutManager = LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
            adapter = mBlogAdapter
        }
        refreshData()
    }

    fun refreshData() {
        mPresenter.getBlogDetails()
    }

    override fun showBlogDetails(blogResponse: HomeResponse) {
        this.mBlogFacts = blogResponse.featured
        mBlogAdapter.setData(blogResponse.featured)
    }
}