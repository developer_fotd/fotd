package com.fotd.ui.fragments.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import com.fotd.ui.base.BaseFragment
import com.indemand.fotd.R
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * Created by Mohit
 */
class HomeFragment : BaseFragment(), HomeInterface.View, View.OnClickListener {
    private lateinit var mPresenter: HomeInterface.PresenterImpl
    private lateinit var mContext: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = HomePresenter(this)
        mPresenter.onAttach()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        refreshData()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        today_iv_share_fact.setOnClickListener(this)
        today_rl_like.setOnClickListener(this)
        today_rl_dislike.setOnClickListener(this)
        today_iv_add_favourite.setOnClickListener(this)
    }

    fun refreshData() {
        mPresenter.getHomeFact()
    }

    override fun setData(userName: String, fact: String, likeResourceId: Int, likeCount: String, dislikeResourceId: Int,
                         dislikeCount: String, favouriteResourceId: Int) {
        today_tv_name.text = userName
        today_tv_fact.text = fact

        today_iv_like.setImageResource(likeResourceId)
        today_iv_dislike.setImageResource(dislikeResourceId)

        today_tv_like_count.text = likeCount
        today_tv_dislike_count.text = dislikeCount

        today_iv_add_favourite.setImageResource(favouriteResourceId)
    }

    override fun shareFact(fact: String) {
        CommonUtil.shareFact(mContext, fact)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.today_iv_share_fact -> {
                mPresenter.shareFact()
                alphaAnimate(today_iv_share_fact)
            }
            R.id.today_rl_like -> {
                alphaAnimate(today_iv_like)
                mPresenter.likeFact(true)
            }
            R.id.today_rl_dislike -> {
                alphaAnimate(today_iv_dislike)
                mPresenter.likeFact(false)
            }
            R.id.today_iv_add_favourite -> {
                alphaAnimate(today_iv_add_favourite)
                mPresenter.addToFavorite()
            }
            else -> {
            }
        }
    }

    private fun alphaAnimate(view: View) {
        val animation = AlphaAnimation(0.2f, 1.0f)
        animation.duration = 300
        view.alpha = 1f
        view.startAnimation(animation)
    }
}