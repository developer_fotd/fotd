package com.fotd.ui.fragments.more

import com.clicklabs.data.network.APIInventory
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.AvatarObj
import com.fotd.data.model.Dependencies
import com.fotd.data.model.UserAPIResponse
import com.fotd.data.model.UserDetails
import com.fotd.data.network.APIKeyConstant
import com.fotd.data.network.APIKeyConstant.*
import com.fotd.data.network.RetrofitUtils
import com.fotd.util.AppConstants
import okhttp3.RequestBody
import java.io.File
import java.util.*

/**
 * Created by Mohit
 */
class MorePresenter(_view: MoreInterface.View) : BasePresenter(), MoreInterface.PresenterImpl {
    private var view: MoreInterface.View = _view
    private val timezoneInfo = java.util.TimeZone.getDefault().toString()
    private val timezoneInMins = TimeZone.getDefault().getRawOffset() / 60000.toLong()
    private lateinit var userDetails: UserDetails;
    private var mListAvatars = arrayListOf<AvatarObj>()

    override fun getLatestData() {
        if (isNetworkConnected()) {
            val mapData =
                    mapOf<String, Any>(API_ACCESS_TOKEN to Dependencies.getAccessToken())

            model.postData(APIInventory.CHECK_ACCESS_TOKEN, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val userAPIResponse = response.toResponseModel(UserAPIResponse::class.java)
                        Dependencies.saveAccessToken(userAPIResponse.userDetails.accessToken)
                        userDetails = userAPIResponse.userDetails
                        Dependencies.saveUserDetails(userAPIResponse.userDetails)

                        var notificationText = ""
                        if (userDetails.notificationEnabled == 1) {
                            notificationText = getString(R.string.profile_tv_notifications_off)
                        } else {
                            notificationText = getString(R.string.profile_tv_notifications_on)
                        }
                        view.setData(userDetails.name, userDetails.email, userDetails.userFactCount.approvedCount,
                                userDetails.userFactCount.pendingCount, userDetails.userFactCount.rejectedCount,
                                userDetails.profileImage, notificationText)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun getListOfAvatars() {
        for (avatar in userDetails.avatars) {
            mListAvatars.add(AvatarObj(avatar, false))
        }

        view.showEditImagePopup(mListAvatars)
    }

    override fun updateUserImage(userImage: File) {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            val mapData =
                    mapOf<String, RequestBody>(
                            API_ACCESS_TOKEN to RetrofitUtils.getRequestBodyFromString(Dependencies.getAccessToken()),
                            API_TIMEZONE to RetrofitUtils.getRequestBodyFromString(timezoneInMins.toString()),
                            API_TIMEZONE_INFO to RetrofitUtils.getRequestBodyFromString(timezoneInfo))

            model.postMultipartData(APIInventory.EDIT_PROFILE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun checkForAvatar() {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else if (!isAvatarSelected()) {
            view.dismissPopup()
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_AVATAR to getSelectedAvatar())

            model.postData(APIInventory.EDIT_PROFILE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        userDetails.setProfileImage(getSelectedAvatar())
                        Dependencies.saveUserDetails(userDetails)
                        view.updateProfilePic(userDetails.profileImage)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun toggleForNotifications() {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            if (userDetails.notificationEnabled == 1) {
                view.toggleNotificationText(getString(R.string.profile_tv_notifications_on))
            } else {
                view.toggleNotificationText(getString(R.string.profile_tv_notifications_off))
            }

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_TIMEZONE to timezoneInMins.toString(),
                            API_TIMEZONE_INFO to timezoneInfo,
                            API_NOTIFICATION_ENABLED to if (userDetails.notificationEnabled == 1) 0 else 1)

            model.postData(APIInventory.EDIT_PROFILE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    userDetails.setNotificationEnabled(if (userDetails.notificationEnabled == 1) 0 else 1)
                    Dependencies.saveUserDetails(userDetails)
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun logout() {
        val logoutRunnable = Runnable {
            if (!isNetworkConnected()) {
                view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
            } else {
                view.showLoading()

                val mapData =
                        mapOf<String, Any>(
                                APIKeyConstant.API_DEVICE_TOKEN to Dependencies.getFCMToken(),
                                APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken())

                model.postData(APIInventory.LOGOUT_USER, mapData, object : BaseInterface.Model.ApiListener {
                    override fun onSuccess(response: CommonResponse) {
                        if (isViewAttached()) {
                            Dependencies.clearData()
                            view.hideLoading()
                            view.showLoginScreen()
                        }
                    }

                    override fun onError(message: String) {
                        if (isViewAttached()) {
                            view.hideLoading()
                            view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                        }
                    }

                    override fun onError() {
                        if (isViewAttached()) {
                            view.hideLoading()
                            view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                        }
                    }
                })
            }
        }

        view.showMessageFromBottom(null, getString(R.string.ask_logout), true,
                getString(R.string.text_cancel), getString(R.string.text_logout), null,logoutRunnable)
    }

    fun isAvatarSelected(): Boolean {
        for (avatar in mListAvatars) {
            if (avatar.isSelected) {
                return true
            }
        }
        return false
    }

    fun getSelectedAvatar(): String {
        for (avatar in mListAvatars) {
            if (avatar.isSelected) {
                return avatar.avatarURL
            }
        }
        return ""
    }
}