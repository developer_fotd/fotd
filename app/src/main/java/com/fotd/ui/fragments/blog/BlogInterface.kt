package com.fotd.ui.fragments.blog

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.HomeResponse


interface BlogInterface {

    interface View : BaseInterface.View {
        fun showBlogDetails(blogResponse: HomeResponse)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun getBlogDetails()
    }

}