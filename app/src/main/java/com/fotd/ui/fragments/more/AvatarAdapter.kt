package com.fotd.ui.fragments.more

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.indemand.fotd.R
import com.fotd.data.model.AvatarObj
import kotlinx.android.synthetic.main.item_avatar.view.*

/**
 * Created by Mohit
 */
open class AvatarAdapter(context: Context, listOfAvatars: ArrayList<AvatarObj>)
    : RecyclerView.Adapter<AvatarAdapter.ViewHolder>() {
    var mContext: Context = context
    var mListOfAvatars = listOfAvatars

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivAvatar = itemView.item_avatar_iv
        val ivAvatarSelected = itemView.item_avatar_iv_selected
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_avatar, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfAvatars.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val avatarDetails = mListOfAvatars.get(position)

        try {
            Glide.with(mContext)
                    .load(avatarDetails.avatarURL)
                    .centerCrop()
                    .into(holder.ivAvatar)
        } catch (e: Exception) {
        }

        if (avatarDetails.isSelected()) {
            holder.ivAvatarSelected.visibility = GONE
        } else {
            holder.ivAvatarSelected.visibility = VISIBLE
        }

        holder.itemView.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        if (!mListOfAvatars.get(holder.getAdapterPosition()).isSelected()) {
                            resetAll()
                            mListOfAvatars.get(holder.getAdapterPosition()).setSelected(true);
                        }
                        notifyDataSetChanged();
                    }
                })
    }

    fun resetAll() {
        for (avatarObject in mListOfAvatars) {
            avatarObject.setSelected(false)
        }
    }
}
