package com.fotd.ui.fragments.more

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.fotd.ui.about.AboutActivity
import com.fotd.ui.add.AddFactActivity
import com.fotd.ui.base.BaseFragment
import com.fotd.ui.changepass.ChangePasswordActivity
import com.fotd.ui.feedback.AddFeedbackActivity
import com.fotd.ui.login.LoginActivity
import com.fotd.ui.myfacts.FavouritesActivity
import com.fotd.ui.myfacts.MyFactsActivity
import com.fotd.ui.search.SearchActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.analytics.FirebaseAnalytics
import com.indemand.fotd.R
import com.fotd.data.model.AvatarObj
import com.fotd.util.AppConstants.EXTRA_FACT_STATUS
import com.fotd.util.AppConstants.FactStatus
import com.fotd.util.CommonUtil
import com.fotd.util.ImageCompressor
import com.fotd.util.ImageCompressor.OnCompressionDone
import kotlinx.android.synthetic.main.dialog_profile_pic.*
import kotlinx.android.synthetic.main.fragment_more.*
import java.io.File

/**
 * Created by Mohit
 */
class MoreFragment : BaseFragment(), MoreInterface.View, View.OnClickListener {
    private lateinit var mPresenter: MoreInterface.PresenterImpl
    private lateinit var mContext: Context
    private lateinit var bsDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = MorePresenter(this)
        mPresenter.onAttach()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        refreshData()
    }

    fun refreshData() {
        mPresenter.getLatestData()
    }

    override fun setData(userName: String, userEmail: String, approvedCount: Int, pendingCount: Int, rejectedCount: Int,
                         userImage: String?, notificationText: String) {
        more_tv_name.text = userName
        more_tv_email.text = userEmail
        more_tv_approved_count.text = approvedCount.toString()
        more_tv_pending_count.text = pendingCount.toString()
        more_tv_discarded_count.text = rejectedCount.toString()
        more_tv_notification.text = notificationText

        try {
            if (!(userImage == null || userImage.isEmpty())) {
                Glide.with(mContext)
                        .load(userImage).placeholder(R.drawable.ic_loading)
                        .into(more_iv_user)
            }
        } catch (e: Exception) {
        }
    }

    override fun showLoginScreen() {
        startActivity(Intent(mContext, LoginActivity::class.java))
    }

    override fun dismissPopup() {
        bsDialog.dismiss()
    }

    override fun updateProfilePic(imageURL: String) {
        try {
            Glide.with(mContext)
                    .load(imageURL).centerCrop().placeholder(R.drawable.ic_loading)
                    .into(more_iv_user)
        } catch (e: Exception) {
        }
    }

    override fun toggleNotificationText(notificationText: String) {
        more_tv_notification.text = notificationText
    }

    override fun showEditImagePopup(listOfAvatars: ArrayList<AvatarObj>) {
        bsDialog = BottomSheetDialog(mContext, R.style.BaseBottomSheetDialogThem)
        bsDialog.setContentView(R.layout.dialog_profile_pic)
        initAvatarList(bsDialog, listOfAvatars)

        bsDialog.dialog_profile_tv_gallery.setOnClickListener {
            bsDialog.dismiss()
            pickImageFromGallery()
        }
        bsDialog.dialog_profile_tv_camera.setOnClickListener {
            bsDialog.dismiss()
            clickImageViaCamera()
        }

        bsDialog.dialog_profile_btn_save.setOnClickListener {
            bsDialog.dismiss()
            mPresenter.checkForAvatar()
        }

        bsDialog.show()
    }

    private fun setListeners() {

        more_iv_user.setOnClickListener(this)
        more_rl_approved.setOnClickListener(this)
        more_rl_discarded.setOnClickListener(this)
        more_rl_pending.setOnClickListener(this)

        menu_btn_add.setOnClickListener(this)
        menu_btn_search.setOnClickListener(this)

        more_tv_favourites.setOnClickListener(this)
        more_tv_feedback.setOnClickListener(this)
        more_tv_share.setOnClickListener(this)
        more_tv_instagram.setOnClickListener(this)
        more_tv_review.setOnClickListener(this)
        more_tv_about.setOnClickListener(this)
        more_tv_password.setOnClickListener(this)
        more_tv_notification.setOnClickListener(this)
        more_tv_signout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.more_iv_user -> if (CommonUtil.isFirstClick()) {
                mPresenter.getListOfAvatars()
            }
            R.id.more_rl_approved -> if (CommonUtil.isFirstClick()) {
                val approvedIntent = Intent(mContext, MyFactsActivity::class.java)
                approvedIntent.putExtra(EXTRA_FACT_STATUS, FactStatus.APPROVED)
                startActivity(approvedIntent)
            }
            R.id.more_rl_discarded -> if (CommonUtil.isFirstClick()) {
                val approvedIntent = Intent(mContext, MyFactsActivity::class.java)
                approvedIntent.putExtra(EXTRA_FACT_STATUS, FactStatus.REJECTED)
                startActivity(approvedIntent)
            }
            R.id.more_rl_pending -> if (CommonUtil.isFirstClick()) {
                val approvedIntent = Intent(mContext, MyFactsActivity::class.java)
                approvedIntent.putExtra(EXTRA_FACT_STATUS, FactStatus.PENDING)
                startActivity(approvedIntent)
            }
            R.id.menu_btn_add -> if (CommonUtil.isFirstClick()) {
                //startActivity(Intent(mContext, AddFactActivity::class.java))
                resultLauncher.launch(Intent(mContext, AddFactActivity::class.java))
            }
            R.id.menu_btn_search -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(mContext, SearchActivity::class.java))
            }
            R.id.more_tv_favourites -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(mContext, FavouritesActivity::class.java))
            }
            R.id.more_tv_feedback -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(mContext, AddFeedbackActivity::class.java))
            }
            R.id.more_tv_instagram -> if (CommonUtil.isFirstClick()) {
                CommonUtil.openInstagram(mContext)
            }
            R.id.more_tv_share -> CommonUtil.shareApp(mContext)
            R.id.more_tv_review -> CommonUtil.openPlayStore(mContext)
            R.id.more_tv_about -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(mContext, AboutActivity::class.java))
            }
            R.id.more_tv_password -> if (CommonUtil.isFirstClick()) {
                startActivity(Intent(mContext, ChangePasswordActivity::class.java))
            }
            R.id.more_tv_notification -> if (CommonUtil.isFirstClick()) {
                mPresenter.toggleForNotifications()
            }
            R.id.more_tv_signout -> {
                mPresenter.logout()
            }
            else -> {
            }
        }
    }

    /**
     * pick an image from gallery
     */
    private fun pickImageFromGallery() {
        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL)
                .folderMode(true)
                .toolbarFolderTitle("Select an Image")
                .toolbarImageTitle("Tap to select")
                .toolbarArrowColor(Color.WHITE)
                .includeVideo(false)
                .single() // single mode
                .showCamera(false)
                .imageDirectory("Camera") //.theme(R.style.CustomImagePickerTheme)
                .enableLog(false)
                .start()
    }

    private fun clickImageViaCamera() {
        ImagePicker.cameraOnly().start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                val image = ImagePicker.getFirstImageOrNull(data)
                var profilePicFile = File(image.path)
                ImageCompressor.Builder(mContext)
                        .setFile(profilePicFile)
                        .setCallback(object : OnCompressionDone {
                            override fun onDone(compressesFiles: ArrayList<File>) {
                                try {
                                    profilePicFile = compressesFiles[0]
                                    Glide.with(mContext)
                                            .load(Uri.fromFile(profilePicFile))
                                            .centerCrop()
                                            .into(more_iv_user)

                                    mPresenter.updateUserImage(profilePicFile)
                                } catch (e: Exception) {
                                    val eventBundle = Bundle()
                                    eventBundle.putString("Exception", "" + e.localizedMessage)
                                    FirebaseAnalytics.getInstance(mContext).logEvent("ImageCrash2", eventBundle)
                                    e.printStackTrace()
                                }
                            }

                            override fun onError(s: String) {}
                        }).build()
            }
        } catch (e: Exception) {
            val eventBundle = Bundle()
            eventBundle.putString("Exception", "" + e.localizedMessage)
            FirebaseAnalytics.getInstance(mContext).logEvent("ImageCrash1", eventBundle)
            e.printStackTrace()
        }
    }

    private fun initAvatarList(bsDialog: BottomSheetDialog, listOfAvatars: ArrayList<AvatarObj>) {
        val blogAdapter = AvatarAdapter(mContext, listOfAvatars)

        val layoutManager = LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
        bsDialog.dialog_profile_rv_avatar.setLayoutManager(layoutManager)
        bsDialog.dialog_profile_rv_avatar.adapter = blogAdapter
    }

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            refreshData()
        }
    }
}