package com.fotd.ui.fragments.home

import com.clicklabs.data.network.APIInventory.Companion.FACT_FAVOURITE
import com.clicklabs.data.network.APIInventory.Companion.FACT_LIKE
import com.clicklabs.data.network.APIInventory.Companion.GET_TODAYS_FACT
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.LikeAPIResponse
import com.fotd.data.model.TodaysAPIResponse
import com.fotd.data.model.UserDetails
import com.fotd.data.network.APIKeyConstant
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.*

/**
 * Created by Mohit
 */
class HomePresenter(_view: HomeInterface.View) : BasePresenter(), HomeInterface.PresenterImpl {
    private var view: HomeInterface.View = _view
    private var mHomeFactDetails: TodaysAPIResponse? = null
    private val userDetails: UserDetails = Dependencies.getUserDetails()

    override fun getHomeFact() {
        if (!isNetworkConnected()) {
            view.showMessageFromBottom(getString(R.string.text_oh_dear), getString(R.string.error_internet_not_connected), false,
                    getString(R.string.text_retry),
                    Runnable {
                        getHomeFact()
                    })
        } else {
            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken())

            model.getData(GET_TODAYS_FACT, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        mHomeFactDetails = response.toResponseModel(TodaysAPIResponse::class.java)
                        parseData()
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun shareFact() {
        mHomeFactDetails?.factDetails?.fact?.let { view.shareFact(it) }
    }

    override fun likeFact(isLiked: Boolean) {
        if (mHomeFactDetails == null) {
            return
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            var status = 0
            if (mHomeFactDetails!!.userLikeStatus == AppConstants.LIKE_STATUS_NOTHING) {
                if (isLiked) {
                    status = LIKE_STATUS_LIKED
                } else {
                    status = LIKE_STATUS_DISLIKED
                }
            } else if (mHomeFactDetails!!.userLikeStatus == AppConstants.LIKE_STATUS_LIKED) {
                if (isLiked) {
                    status = LIKE_STATUS_NOTHING
                } else {
                    status = LIKE_STATUS_DISLIKED
                }
            } else {
                if (isLiked) {
                    status = LIKE_STATUS_LIKED
                } else {
                    status = LIKE_STATUS_NOTHING
                }
            }

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            APIKeyConstant.API_FACT_ID to mHomeFactDetails!!.factDetails.factId,
                            APIKeyConstant.API_STATUS to status)

            model.postData(FACT_LIKE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val likeAPIResponse: LikeAPIResponse = response.toResponseModel(LikeAPIResponse::class.java)
                        mHomeFactDetails!!.setLikeDislikeResponse(status, likeAPIResponse.likeCount, likeAPIResponse.dislikeCount)
                        parseData()
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun addToFavorite() {
        if (mHomeFactDetails == null) {
            return
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {

            var status = 0
            if (mHomeFactDetails!!.userFavStatus == 0) {
                status = 1
            }

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            APIKeyConstant.API_FACT_ID to mHomeFactDetails!!.factDetails.factId,
                            APIKeyConstant.API_STATUS to status)

            model.postData(FACT_FAVOURITE, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        mHomeFactDetails!!.setUserFavStatus(status)
                        parseData()
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    fun parseData() {

        var likeResourceId = R.drawable.ic_like_default_white
        var dislikeResourceId = R.drawable.ic_dislike_default_white
        var favResourceId = R.drawable.ic_fav_default_white
        if (mHomeFactDetails!!.userLikeStatus == LIKE_STATUS_LIKED) {
            likeResourceId = R.drawable.ic_like_pressed_white
        } else if (mHomeFactDetails!!.userLikeStatus == LIKE_STATUS_DISLIKED) {
            dislikeResourceId = R.drawable.ic_dislike_pressed_white
        }

        if (mHomeFactDetails!!.userFavStatus == 1) {
            favResourceId = R.drawable.ic_fav_pressed
        }

        view.setData(userDetails.name.split(" ")[0], mHomeFactDetails!!.factDetails.fact, likeResourceId,
                mHomeFactDetails!!.likeCount.toString(), dislikeResourceId,
                mHomeFactDetails!!.dislikeCount.toString(), favResourceId)
    }

}