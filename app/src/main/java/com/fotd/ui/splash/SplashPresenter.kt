package com.fotd.ui.splash

import android.os.Handler
import com.clicklabs.data.network.APIInventory.Companion.CHECK_ACCESS_TOKEN
import com.clicklabs.data.network.APIInventory.Companion.CHECK_APP_VERSION
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BaseModel
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.BuildConfig
import com.indemand.fotd.R
import com.fotd.data.model.AppVersionDtls
import com.fotd.data.model.Dependencies
import com.fotd.data.model.UserAPIResponse
import com.fotd.data.network.APIKeyConstant.*
import com.fotd.fcm.FCMTokenInterface
import com.fotd.fcm.MyFirebaseMessagingService
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.DEVICE_TYPE_ANDROID


class SplashPresenter(_view: SplashInterface.View) : BasePresenter(), SplashInterface.PresenterImpl,
        FCMTokenInterface {
    private var isTimerStopped = false
    private var isAppVersionChecked = false
    private lateinit var appVersionDtls: AppVersionDtls
    private lateinit var message: String
    private var isTokenReceived = false
    override var model: BaseModel = BaseModel();
    private var view: SplashInterface.View = _view

    override fun init() {
        isAppVersionChecked = false;
        startTimer()
        if (isNetworkConnected()) {
            MyFirebaseMessagingService.setCallback(this)
            checkAppVersion()
        } else {
            val runnable = Runnable {
                init()
            }
            view.showMessageFromBottom(getString(R.string.text_oh_dear), getString(R.string.error_internet_not_connected),
                    false, getString(R.string.text_retry), runnable)
        }
    }

    fun checkAppVersion() {
        val mapData =
                mapOf<String, Any>(
                        API_APP_VERSION to BuildConfig.VERSION_CODE,
                        API_DEVICE_TYPE to AppConstants.DEVICE_TYPE_ANDROID)

        model.getData(CHECK_APP_VERSION, mapData, object : BaseInterface.Model.ApiListener {
            override fun onSuccess(response: CommonResponse) {
                if (isViewAttached()) {
                    isAppVersionChecked = true
                    message = response.message!!
                    appVersionDtls = response.toResponseModel(AppVersionDtls::class.java)
                    Dependencies.saveAppData(appVersionDtls)
                    checkForAllProcesses()
                }
            }

            override fun onError(message: String) {
                view.showLoginScreen()
            }

            override fun onError() {
                view.showLoginScreen()
            }
        })
    }

    override fun onFailure() {

    }

    override fun onTokenReceived(token: String?) {
        isTokenReceived = true
        Dependencies.saveFCMToken(token)
        checkForAllProcesses()
    }

    fun checkForAllProcesses() {
        if (isTimerStopped && isTokenReceived && isAppVersionChecked) {
            if (appVersionDtls.isForceUpdate) {
                val runnableUpdate = Runnable {
                    view.openPlayStore(appVersionDtls.packageName, appVersionDtls.link)
                }
                view.showMessageFromBottom(getString(R.string.text_new_update), message, false,
                        getString(R.string.text_update), runnableUpdate)
            } else if (appVersionDtls.isManualUpdate) {
                val runnableUpdate = Runnable {
                    view.openPlayStore(appVersionDtls.packageName, appVersionDtls.link)
                }
                val runnableSkip = Runnable {
                    checkAccessToken()
                }

                view.showMessageFromBottom(getString(R.string.text_new_update), message, false,
                        getString(R.string.text_update), getString(R.string.text_not_now), runnableUpdate, runnableSkip)
            } else {
                checkAccessToken()
            }
        }
    }

    fun checkAccessToken() {
        if (Dependencies.getAccessToken().isEmpty()) {
            Dependencies.clearData()
            view.showLoginScreen()
        } else {
            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_DEVICE_TOKEN to Dependencies.getFCMToken(),
                            API_DEVICE_TYPE to DEVICE_TYPE_ANDROID,
                            API_DEVICE_NAME to "ANDROID")


            model.postData(CHECK_ACCESS_TOKEN, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val userAPIResponse: UserAPIResponse = response.toResponseModel(UserAPIResponse::class.java)
                        Dependencies.saveAccessToken(userAPIResponse.userDetails.accessToken)
                        Dependencies.saveUserDetails(userAPIResponse.userDetails)
                        Dependencies.saveGuestDtls(false)
                        view.showHomeScreen()
                    }
                }

                override fun onError(message: String) {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.showLoginScreen()
                    }
                }

                override fun onError() {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.showLoginScreen()
                    }
                }
            })
        }
    }

    fun startTimer() {
        if (!isTimerStopped) {
            Handler().postDelayed({
                isTimerStopped = true
                checkForAllProcesses()
            }, 1000)
        }
    }
}