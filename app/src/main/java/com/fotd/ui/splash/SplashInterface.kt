package com.fotd.ui.splash

import com.fotd.ui.base.BaseInterface


interface SplashInterface {

    interface View : BaseInterface.View {

        fun showLoginScreen()

        fun showHomeScreen()

        fun openPlayStore(packageName: String, link: String)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun init()
    }
}