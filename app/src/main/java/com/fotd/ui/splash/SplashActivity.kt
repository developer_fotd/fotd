package com.fotd.ui.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.fotd.ui.HomeActivity
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.login.LoginActivity
import com.indemand.fotd.R
import com.fotd.util.AppConstants


class SplashActivity : BaseActivity(), SplashInterface.View {
    private lateinit var mPresenter: SplashInterface.PresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mPresenter = SplashPresenter(this)
        mPresenter.onAttach()
        mPresenter.init()
    }

    override fun showLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun showHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    override fun openPlayStore(packageName: String, link: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("market://details?id=$packageName")
        try {
            startActivityForResult(intent, AppConstants.REQUEST_CODE_PLAY_STORE)
        } catch (e: Exception) {
            intent.data = Uri.parse(link)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.REQUEST_CODE_PLAY_STORE) {
            mPresenter.init()
        }
    }
}