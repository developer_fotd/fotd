package com.fotd.ui

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.Fragment
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.fragments.blog.BlogFragment
import com.fotd.ui.fragments.home.HomeFragment
import com.fotd.ui.fragments.more.MoreFragment
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : BaseActivity(), View.OnClickListener {
    private var doubleBackToExitPressedOnce = false
    private val BACK_PRESS_TIME_IN_MS = 2000
    private val mBlogFragment = BlogFragment()
    private val mHomeFragment = HomeFragment()
    private val mMoreFragment = MoreFragment()

    //init views
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListeners()
        openFragment(mHomeFragment)
        checkForAd()
        /*if (Dependencies.getRatingCounter(this) > 4 && !Dependencies.isRated(this)) {
            showRatingPopup()
        }*/
    }

    private fun setListeners() {
        home_rl_blog.setOnClickListener(this)
        home_rl_today.setOnClickListener(this)
        home_rl_more.setOnClickListener(this)
    }

    private fun checkForAd() {
        try {
            if (Dependencies.getAppData().adMobEnabled == 1) {
                //loadFBAd()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*fun loadFBAd() {
        //AdSettings.addTestDevice("HASHED ID")
        val interstitialAd = com.facebook.ads.InterstitialAd(this, getString(R.string.fb_inter_id))

        val adListener = object : com.facebook.ads.InterstitialAdListener {
            override fun onLoggingImpression(p0: Ad?) {
            }

            override fun onInterstitialDisplayed(p0: Ad?) {
            }

            override fun onAdClicked(p0: Ad?) {
            }

            override fun onInterstitialDismissed(p0: Ad?) {

            }

            override fun onError(p0: Ad?, p1: AdError?) {
                loadGoogleAd()
            }

            override fun onAdLoaded(p0: Ad?) {
                // Show the ad
                interstitialAd.show()
            }
        }

        val loadAdConfig = interstitialAd.buildLoadAdConfig()
                .withAdListener(adListener)
                .build()

        interstitialAd.loadAd(loadAdConfig)
    }*/

    /*fun loadGoogleAd() {
        MobileAds.initialize(this, getString(R.string.admob_app_id))
        val mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.admob_inter_id)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                mInterstitialAd.show()
            }
        }
    }*/

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        //transaction.replace(R.id.home_fl_container, fragment)
        if (fragment.isAdded) {
            if (fragment is HomeFragment) {
                transaction.hide(mBlogFragment)
                transaction.hide(mMoreFragment)

                transaction.show(mHomeFragment)
                mHomeFragment.refreshData()
            } else if (fragment is BlogFragment) {
                transaction.hide(mHomeFragment)
                transaction.hide(mMoreFragment)

                transaction.show(mBlogFragment)
                mBlogFragment.refreshData()
            } else if (fragment is MoreFragment) {
                transaction.hide(mHomeFragment)
                transaction.hide(mBlogFragment)

                transaction.show(mMoreFragment)
                mMoreFragment.refreshData()
            }
        } else {
            transaction.add(R.id.home_fl_container, fragment)
            transaction.addToBackStack(null)
            transaction.show(fragment)
        }
        transaction.commit()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true)
            return
        }
        this.doubleBackToExitPressedOnce = true
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, BACK_PRESS_TIME_IN_MS.toLong())
    }

    override fun onClick(v: View) {
        checkForIcon(v)
        when (v.getId()) {
            R.id.home_rl_blog -> {
                openFragment(mBlogFragment)
            }

            R.id.home_rl_today -> {
                openFragment(mHomeFragment)
            }

            R.id.home_rl_more -> {
                openFragment(mMoreFragment)
            }
        }
    }

    private fun checkForIcon(v: View) {
        home_tv_blog.visibility = GONE
        home_tv_today.visibility = GONE
        home_tv_more.visibility = GONE

        home_rl_blog.background = null
        home_rl_today.background = null
        home_rl_more.background = null

        when (v.getId()) {
            R.id.home_rl_blog -> {
                home_tv_blog.visibility = VISIBLE
                home_rl_blog.setBackgroundResource(R.drawable.bg_selected_tab)
            }

            R.id.home_rl_today -> {
                home_tv_today.visibility = VISIBLE
                home_rl_today.setBackgroundResource(R.drawable.bg_selected_tab)
            }

            R.id.home_rl_more -> {
                home_tv_more.visibility = VISIBLE
                home_rl_more.setBackgroundResource(R.drawable.bg_selected_tab)
            }
        }
    }
}