package com.fotd.ui.otp

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.password.ResetActivity
import com.indemand.fotd.R
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.EXTRA_ACCESS_TOKEN
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.activity_signup.*


class OTPActivity : BaseActivity(), OTPInterface.View, View.OnClickListener {
    private lateinit var mPresenter: OTPInterface.PresenterImpl
    private lateinit var mEmail: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        setListeners()
        getIntentData()

        mPresenter = OTPPresenter(this)
        mPresenter.onAttach()
    }

    private fun setListeners() {
        otp_btn.setOnClickListener(this)
        otp_tv_resend.setOnClickListener(this)
        otp_tv_back.setOnClickListener(this)

        CommonUtil.moveFrontAndBack(1, otp_et_1, otp_et_2, otp_et_3, otp_et_4);
    }

    private fun getIntentData() {
        mEmail = intent.getStringExtra(AppConstants.EXTRA_EMAIL)!!
    }


    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    override fun resetAllTheFields() {
        otp_et_1.requestFocus()
        otp_et_1.text?.clear()
        otp_et_2.text?.clear()
        otp_et_3.text?.clear()
        otp_et_4.text?.clear()
    }

    override fun showResetPassword(access: String) {
        val intent = Intent(this, ResetActivity::class.java)
        intent.putExtra(EXTRA_ACCESS_TOKEN, access)
        startActivity(intent)
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.otp_tv_resend -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                mPresenter.resendOTP(mEmail)
            }
            R.id.otp_btn -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                mPresenter.verifyOTP(mEmail, otp_et_1.text.toString(), otp_et_2.text.toString(), otp_et_3.text.toString(),
                        otp_et_4.text.toString())
            }

            R.id.otp_tv_back -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                finish()
            }
        }
    }
}