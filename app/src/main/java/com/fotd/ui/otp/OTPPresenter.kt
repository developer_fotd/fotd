package com.fotd.ui.otp

import com.clicklabs.data.network.APIInventory
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.OTPAPIResponse
import com.fotd.data.network.APIKeyConstant.API_EMAIL
import com.fotd.data.network.APIKeyConstant.API_OTP
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.SnackBarType


class OTPPresenter(_view: OTPInterface.View) : BasePresenter(), OTPInterface.PresenterImpl {
    private var view: OTPInterface.View = _view

    override fun resendOTP(email: String) {
        if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_EMAIL to email)

            model.postData(APIInventory.VERIFY_OTP, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.resetAllTheFields()
                    }
                }

                override fun onError(message: String) {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    Dependencies.clearData()
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    override fun verifyOTP(email: String, otp1: String?, otp2: String?, otp3: String?, otp4: String?) {
        if (otp1!!.isEmpty() || otp2!!.isEmpty() || otp3!!.isEmpty() || otp4!!.isEmpty()) {
            view.showSnackbar(R.string.err_no_otp, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            view.showLoading()

            val otpString = otp1 + otp2 + otp3 + otp4
            val mapData =
                    mapOf<String, Any>(
                            API_EMAIL to email,
                            API_OTP to otpString)

            model.postData(APIInventory.VERIFY_OTP, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        val otpResponse = response.toResponseModel(OTPAPIResponse::class.java)
                        view.hideLoading()
                        view.showResetPassword(otpResponse.accessToken)
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, AppConstants.SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, AppConstants.SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}