package com.fotd.ui.otp

import com.fotd.ui.base.BaseInterface


interface OTPInterface {

    interface View : BaseInterface.View {

        fun resetAllTheFields()

        fun showResetPassword(access: String)
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun resendOTP(email: String)

        fun verifyOTP(email: String, otp1: String?, otp2: String?, otp3: String?, otp4: String?)
    }
}