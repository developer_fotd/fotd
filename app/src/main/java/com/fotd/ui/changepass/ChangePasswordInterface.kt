package com.fotd.ui.changepass

import com.fotd.ui.base.BaseInterface


interface ChangePasswordInterface {

    interface View : BaseInterface.View {

        fun closeView()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun checkAndChange(oldPassword: String, newPassword: String)
    }
}