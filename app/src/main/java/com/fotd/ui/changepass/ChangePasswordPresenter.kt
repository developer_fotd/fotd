package com.fotd.ui.changepass

import com.clicklabs.data.network.APIInventory.Companion.CHANGE_PASSWORD
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.network.APIKeyConstant.*
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.SnackBarType


class ChangePasswordPresenter(_view: ChangePasswordInterface.View) : BasePresenter(), ChangePasswordInterface.PresenterImpl {
    private var view: ChangePasswordInterface.View = _view

    override fun checkAndChange(oldPassword: String, newPassword: String) {
        if (oldPassword.isEmpty()) {
            view.showSnackbar(R.string.err_no_old_password, SnackBarType.ERROR)
        } else if (newPassword.isEmpty()) {
            view.showSnackbar(R.string.err_no_new_password, SnackBarType.ERROR)
        } else if (newPassword.trim().length < AppConstants.PASS_MIN_LENGTH) {
            view.showSnackbar(R.string.err_invalid_password, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_OLD_PASSWORD to oldPassword,
                            API_NEW_PASSWORD to newPassword)

            model.postData(CHANGE_PASSWORD, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.closeView()
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}