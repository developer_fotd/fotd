package com.fotd.ui.changepass

import android.os.Bundle
import android.view.View
import com.fotd.ui.base.BaseActivity
import com.indemand.fotd.R
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_change_pass.*
import kotlinx.android.synthetic.main.activity_signup.*


class ChangePasswordActivity : BaseActivity(), ChangePasswordInterface.View, View.OnClickListener {
    private lateinit var mPresenter: ChangePasswordInterface.PresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pass)
        setListeners()

        mPresenter = ChangePasswordPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        change_pass_btn_submit.setOnClickListener(this)
        toolbar_ll_main.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.change_pass_btn_submit -> mPresenter.checkAndChange(change_pass_et_old.text.toString().trim(),
                    change_pass_et_new.text.toString().trim())

            R.id.toolbar_ll_main -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                finish()
            }
        }
    }

    override fun closeView() {
        finish()
    }
}