package com.fotd.ui.email

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.fotd.ui.base.BaseActivity
import com.fotd.ui.otp.OTPActivity
import com.indemand.fotd.R
import com.fotd.util.AppConstants.EXTRA_EMAIL
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_email.*
import kotlinx.android.synthetic.main.activity_signup.*


class EmailActivity : BaseActivity(), EmailInterface.View, View.OnClickListener {
    private lateinit var mPresenter: EmailInterface.PresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email)
        setListeners()

        mPresenter = EmailPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        email_btn.setOnClickListener(this)
        email_tv_back.setOnClickListener(this)
    }

    override fun showNextScreen() {
        val intent = Intent(this, OTPActivity::class.java)
        intent.putExtra(EXTRA_EMAIL.toString(), email_et_email.text.toString().trim())
        startActivity(intent)
    }

    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.email_btn -> mPresenter.sendResetPasswordLink(email_et_email.text.toString().trim())

            R.id.email_tv_back -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                finish()
            }
        }
    }
}