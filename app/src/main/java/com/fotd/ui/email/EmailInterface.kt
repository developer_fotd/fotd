package com.fotd.ui.email

import com.fotd.ui.base.BaseInterface


interface EmailInterface {

    interface View : BaseInterface.View {

        fun showNextScreen()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun sendResetPasswordLink(email: String)
    }
}