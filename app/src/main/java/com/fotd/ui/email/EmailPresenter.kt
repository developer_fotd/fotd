package com.fotd.ui.email

import com.clicklabs.data.network.APIInventory.Companion.SEND_PASSWORD_LINK
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.network.APIKeyConstant
import com.fotd.util.AppConstants.SnackBarType
import com.fotd.util.CommonUtil


class EmailPresenter(_view: EmailInterface.View) : BasePresenter(), EmailInterface.PresenterImpl {
    private var view: EmailInterface.View = _view

    override fun sendResetPasswordLink(email: String) {
        if (CommonUtil.isInvalidEmail(email)) {
            view.showSnackbar(R.string.err_invalid_email, SnackBarType.ERROR)
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            APIKeyConstant.API_EMAIL to email)

            model.postData(SEND_PASSWORD_LINK, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showNextScreen()
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }
}