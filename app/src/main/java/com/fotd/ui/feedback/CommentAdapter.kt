package com.fotd.ui.feedback

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.indemand.fotd.R
import com.fotd.data.model.CommentObj
import kotlinx.android.synthetic.main.item_comment.view.*

/**
 * Created by Mohit
 */
open class CommentAdapter(context: Context) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {
    var mContext: Context = context
    var mListOfComments = arrayListOf<CommentObj>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvComment = itemView.item_comment_tv
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_comment, parent, false))
    }

    override fun getItemCount(): Int {
        return mListOfComments.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val commentDetails = mListOfComments.get(position)
        holder.tvComment.text = commentDetails.comment

        if (commentDetails.isSelected()) {
            holder.tvComment.setBackgroundColor(Color.WHITE)
            holder.tvComment.setTextColor(mContext.resources.getColor(R.color.bg_tabs))
        } else {
            holder.tvComment.setBackgroundColor(mContext.resources.getColor(R.color.bg_tabs))
            holder.tvComment.setTextColor(Color.WHITE)
        }

        holder.itemView.setOnClickListener(
                object : View.OnClickListener {
                    override fun onClick(v: View) {
                        if (mListOfComments.get(holder.getAdapterPosition()).isSelected()) {
                            mListOfComments.get(holder.getAdapterPosition()).setSelected(false);
                        } else {
                            mListOfComments.get(holder.getAdapterPosition()).setSelected(true);
                        }
                        notifyDataSetChanged();
                    }
                })
    }

    fun setData(listOfComments: ArrayList<CommentObj>) {
        mListOfComments = listOfComments
        notifyDataSetChanged()
    }
}
