package com.fotd.ui.feedback

import android.os.Bundle
import android.view.View
import com.fotd.ui.base.BaseActivity
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.indemand.fotd.R
import com.fotd.data.model.CommentObj
import com.fotd.util.CommonUtil
import kotlinx.android.synthetic.main.activity_feedback.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_add_fact.toolbar_ll_main


class AddFeedbackActivity : BaseActivity(), AddFeedbackInterface.View, View.OnClickListener {
    private lateinit var mPresenter: AddFeedbackInterface.PresenterImpl

    private lateinit var mAdapter: CommentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)
        setListeners()
        initRecyclerView()

        mPresenter = AddFeedbackPresenter(this)
        mPresenter.onAttach()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDetach()
    }

    fun setListeners() {
        feedback_iv_rate_1.setOnClickListener(this)
        feedback_iv_rate_2.setOnClickListener(this)
        feedback_iv_rate_3.setOnClickListener(this)

        feedback_btn_submit.setOnClickListener(this)
        toolbar_ll_main.setOnClickListener(this)
    }

    private fun initRecyclerView() {
        val layoutManager = FlexboxLayoutManager(this);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);

        feedback_rv_options.setLayoutManager(layoutManager)
        mAdapter = CommentAdapter(this)
        feedback_rv_options.setAdapter(mAdapter)
    }


    override fun onClick(v: View) {
        when (v.getId()) {
            R.id.feedback_iv_rate_1 -> {
                mPresenter.getListOfOptions(1)
            }

            R.id.feedback_iv_rate_2 -> {
                mPresenter.getListOfOptions(2)
            }

            R.id.feedback_iv_rate_3 -> {
                mPresenter.getListOfOptions(3)
            }

            R.id.feedback_btn_submit -> mPresenter.submitFeedback(feedback_et_description.text.toString().trim())

            R.id.toolbar_ll_main -> {
                CommonUtil.hideKeyboard(this, register_et_user_name)
                finish()
            }
        }
    }

    override fun markAllAsUnselected() {
        feedback_iv_rate_1.setImageResource(R.drawable.ic_star_default);
        feedback_iv_rate_2.setImageResource(R.drawable.ic_star_default);
        feedback_iv_rate_3.setImageResource(R.drawable.ic_star_default);
    }

    override fun markFirstSelected() {
        feedback_iv_rate_1.setImageResource(R.drawable.ic_star_pressed)
    }

    override fun markSecondSelected() {
        feedback_iv_rate_1.setImageResource(R.drawable.ic_star_pressed)
        feedback_iv_rate_2.setImageResource(R.drawable.ic_star_pressed)
    }

    override fun markThirdSelected() {
        feedback_iv_rate_1.setImageResource(R.drawable.ic_star_pressed)
        feedback_iv_rate_2.setImageResource(R.drawable.ic_star_pressed)
        feedback_iv_rate_3.setImageResource(R.drawable.ic_star_pressed)
    }

    override fun setListOfOptions(listOfOptions: ArrayList<CommentObj>) {
        feedback_rv_options.visibility = View.VISIBLE
        mAdapter.setData(listOfOptions)
    }

    override fun closeView() {
        finish()
    }
}