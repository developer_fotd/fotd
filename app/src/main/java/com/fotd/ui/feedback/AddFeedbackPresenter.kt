package com.fotd.ui.feedback

import com.clicklabs.data.network.APIInventory
import com.clicklabs.data.network.CommonResponse
import com.fotd.ui.base.BaseInterface
import com.fotd.ui.base.BasePresenter
import com.indemand.fotd.R
import com.fotd.data.model.Dependencies
import com.fotd.data.model.CommentObj
import com.fotd.data.network.APIKeyConstant.*
import com.fotd.util.AppConstants
import com.fotd.util.AppConstants.DEVICE_TYPE_ANDROID
import com.fotd.util.AppConstants.SnackBarType


class AddFeedbackPresenter(_view: AddFeedbackInterface.View) : BasePresenter(), AddFeedbackInterface.PresenterImpl {
    private var view: AddFeedbackInterface.View = _view

    private val mListComments = arrayListOf<CommentObj>()
    private var mRating: Int = -1

    override fun getListOfOptions(rating: Int) {
        mRating = rating;
        mListComments.clear()
        view.markAllAsUnselected()
        if (rating == 1) {
            mListComments.add(CommentObj("wrong information"))
            mListComments.add(CommentObj("grammar issues"))
            mListComments.add(CommentObj("buggy"))
            mListComments.add(CommentObj("design issues"))
            mListComments.add(CommentObj("not compatible"))
            mListComments.add(CommentObj("other"))

            view.markFirstSelected()
        } else if (rating == 2) {
            mListComments.add(CommentObj("irrelevant design"))
            mListComments.add(CommentObj("less data"))
            mListComments.add(CommentObj("lagging"))
            mListComments.add(CommentObj("grammar issues"))
            mListComments.add(CommentObj("tough to use"))
            mListComments.add(CommentObj("other"))

            view.markSecondSelected()
        } else if (rating == 3) {
            mListComments.add(CommentObj("design"))
            mListComments.add(CommentObj("easy to use"))
            mListComments.add(CommentObj("relevant facts"))
            mListComments.add(CommentObj("other"))

            view.markThirdSelected()
        }

        view.setListOfOptions(mListComments)
    }

    override fun submitFeedback(feedback: String) {
        if (mRating == -1) {
            view.showSnackbar(R.string.feedback_no_rating, SnackBarType.ERROR)
        } else if (!isCommentsSelected()) {
            if (mRating == 1) {
                view.showSnackbar(R.string.feedback_mesaage_poor, SnackBarType.ERROR)
            } else if (mRating == 2) {
                view.showSnackbar(R.string.feedback_mesaage_medium, SnackBarType.ERROR)
            } else {
                view.showSnackbar(R.string.feedback_mesaage_best, SnackBarType.ERROR)
            }
        } else if (!isNetworkConnected()) {
            view.showSnackbar(R.string.error_no_internet, AppConstants.SnackBarType.ERROR)
        } else {
            view.showLoading()

            val mapData =
                    mapOf<String, Any>(
                            API_ACCESS_TOKEN to Dependencies.getAccessToken(),
                            API_RATING to mRating,
                            API_COMMENTS to getSelectedComments(),
                            API_FEEDBACK to feedback,
                            API_DEVICE_TYPE to DEVICE_TYPE_ANDROID)

            model.postData(APIInventory.ADD_FEEDBACK, mapData, object : BaseInterface.Model.ApiListener {
                override fun onSuccess(response: CommonResponse) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showMessageFromBottom(getString(R.string.feedback_success_title), getString(R.string.feedback_success_message),
                                false, getString(R.string.text_okay), Runnable {
                            view.closeView()
                        })
                    }
                }

                override fun onError(message: String) {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(message, SnackBarType.ERROR)
                    }
                }

                override fun onError() {
                    if (isViewAttached()) {
                        view.hideLoading()
                        view.showSnackbar(R.string.please_try_again, SnackBarType.ERROR)
                    }
                }
            })
        }
    }

    private fun isCommentsSelected(): Boolean {
        for (commentObj in mListComments) {
            if (commentObj.isSelected) {
                return true
            }
        }
        return false
    }

    private fun getSelectedComments(): String {
        val stringBuffer = StringBuffer()
        for (commentObj in mListComments) {
            if (commentObj.isSelected) {
                stringBuffer.append(commentObj.comment)
                stringBuffer.append(", ")
            }
        }
        return stringBuffer.toString()
    }
}