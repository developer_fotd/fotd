package com.fotd.ui.feedback

import com.fotd.ui.base.BaseInterface
import com.fotd.data.model.CommentObj


interface AddFeedbackInterface {

    interface View : BaseInterface.View {

        fun markAllAsUnselected()

        fun markFirstSelected()

        fun markSecondSelected()

        fun markThirdSelected()

        fun setListOfOptions(listOfOptions: ArrayList<CommentObj>)

        fun closeView()
    }

    interface PresenterImpl : BaseInterface.PresenterImpl {
        fun getListOfOptions(rating: Int)

        fun submitFeedback(fact: String)
    }
}