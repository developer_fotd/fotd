package com.fotd.util;

/**
 * Created by Click Labs © 2019. ALL RIGHTS RESERVED.
 */
public interface AppConstants {
    int REQUEST_CODE_PLAY_STORE = 10101;
    int PASS_MIN_LENGTH = 8;

    int DEVICE_TYPE_ANDROID = 2; //for android

    int LIKE_STATUS_LIKED = 1;
    int LIKE_STATUS_DISLIKED = 0;
    int LIKE_STATUS_NOTHING = 2;


    int FAV_STATUS_DEFAULT = 0;
    int FAV_STATUS_PRESSED = 1;

    int SEARCH_FACTS = 0;
    int BLOG_FACTS = 3;
    int MY_FACTS = 2;
    int FAVOURITE_FACTS = 4;

    int SUCCESS_CODE = 200;
    int LIMIT_DATA = 10;

    String EXTRA_EMAIL = "extra_email";
    String EXTRA_ACCESS_TOKEN = "extra_access_token";
    String EXTRA_FACT_ID = "extra_fact_id";
    String EXTRA_ALL_FACTS = "extra_all_facts";
    String EXTRA_CLICKED_POSITION = "extra_clicked_position";
    String EXTRA_IS_FROM_FAVORITE = "extra_is_from_favorite";
    String EXTRA_IS_FROM_SEARCH = "extra_is_from_search";
    String EXTRA_IS_FROM_MY_FACTS = "extra_is_from_my_facts";
    String EXTRA_FACT_TYPE = "extra_fact_type";
    String EXTRA_FACT_STATUS = "extra_fact_status";
    String EXTRA_FACT_SEARCH = "extra_fact_search";

    String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String UI_FORMAT_DATE = "dd MMM, yyyy";
    String UI_FORMAT_TIME = "HH:mm";

    int[] COLOR_RED = {0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96, 102, 108, 114, 120};
    int[] COLOR_L_BLUE = {1, 7, 13, 19, 25, 31, 37, 43, 49, 55, 61, 67, 73, 79, 85, 91, 97, 103, 109, 115, 121};
    int[] COLOR_D_GREEN = {2, 8, 14, 20, 26, 32, 38, 44, 50, 56, 62, 68, 74, 80, 86, 92, 98, 104, 110, 116, 122};
    int[] COLOR_D_BLUE = {3, 9, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75, 81, 87, 93, 99, 105, 111, 117, 123};
    int[] COLOR_YELLOW = {4, 10, 16, 22, 28, 34, 40, 46, 52, 58, 64, 70, 76, 82, 88, 94, 100, 106, 112, 118, 124};
    int[] COLOR_L_GREEN = {5, 11, 17, 23, 29, 35, 41, 47, 53, 59, 65, 71, 77, 83, 89, 95, 101, 107, 113, 119, 125};

    interface SnackBarType {
        int ERROR = 0;
        int SUCCESS = 1;
        int MESSAGE = 2;
    }

    interface FactStatus {
        int PENDING = 0;
        int APPROVED = 1;
        int REJECTED = 2;
    }
}
