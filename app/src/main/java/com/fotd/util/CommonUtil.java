package com.fotd.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.fotd.data.model.AppVersionDtls;
import com.fotd.data.model.Dependencies;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import androidx.core.app.ShareCompat;

import static com.fotd.MyApplication.getAppContext;
import static com.fotd.util.AppConstants.ISO_FORMAT;
import static com.fotd.util.AppConstants.UI_FORMAT_DATE;

/**
 * Created by Click Labs © 2019. ALL RIGHTS RESERVED.
 */
public final class CommonUtil {
    private static long mLastClickTime = 0;

    /**
     * Prevent instantiation
     */
    private CommonUtil() {
    }

    /**
     * Method to check if internet is connected
     *
     * @return true if connected to any network else return false
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Method to check if internet is connected
     *
     * @param context context of calling class
     * @return true if connected to any network else return false
     */
    public static boolean isNetworkAvailable(final Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) (context.getApplicationContext()).getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Method to show toast
     *
     * @param message  that display in the Toast
     * @param mContext context of calling activity or fragment
     */
    public static void showToast(final Context mContext, final String message) {
        if (mContext == null
                || message == null
                || message.isEmpty()) {
            return;
        }
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }


    /**
     * @param context of calling activity or fragment
     * @return pixel scale factor
     */
    private static float getPixelScaleFactor(final Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT;
    }


    /**
     * Method to convert dp into pixel
     *
     * @param context of calling activity or fragment
     * @param dp      dp value that need to convert into px
     * @return px converted dp into px
     */
    public static int dpToPx(final Context context, final int dp) {
        return Math.round(dp * getPixelScaleFactor(context));
    }

    /**
     * Method to convert pixel into dp
     *
     * @param context of calling activity or fragment
     * @param px      pixel value that need to convert into dp
     * @return dp converted px into dp
     */
    public static int pxToDp(final Context context, final int px) {
        return Math.round(px / getPixelScaleFactor(context));
    }

    /**
     * Get the app version code
     *
     * @param context calling context
     * @return the app version code
     */
    public static int getAppVersionCode(final Context context) {
        try {
            return context.getApplicationContext().getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return 0;
    }


    /**
     * Get base 64 encoded string
     *
     * @param file the file
     * @return base64 encoded string
     */
    public static String getBase64EncodedString(final File file) {

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        byte[] bytes;
        byte[] buffer = new byte[(int) file.length()];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bytes = output.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    /**
     * Method to valid email
     *
     * @param email email
     * @return true if valid else false
     */
    public static boolean isInvalidEmail(final String email) {
        if (email.isEmpty()) {
            return true;
        }
        if (!email.matches(Patterns.EMAIL_ADDRESS.toString())) {
            return true;
        }
        return false;
    }

    /**
     * Hide keyboard.
     *
     * @param context the context
     * @param view    the view
     */
    public static void hideKeyboard(final Context context, final View view) {
        if (context != null) {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)
                        ((Activity) context).getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            } else {
                hideKeyboard(context);
            }
        }
    }

    /**
     * Hide keyboard.
     *
     * @param context the context
     */
    public static void hideKeyboard(final Context context) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null && ((Activity) context).getCurrentFocus() != null) {
                imm.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
                ((Activity) context).getCurrentFocus().clearFocus();
            }
        }
    }

    public static void shareApp(final Context context) {
        String shareText = "hey! check this app out:" + "\n"
                + "play store:"
                + "\n" + "https://play.google.com/store/apps/details?id=com.indemand.fotd"
                + "\n" + "\n" + "app store:"
                + "\n" + "https://itunes.apple.com/az/app/fact-of-the-day-new-facts/id1450758752?mt=8";

        ShareCompat.IntentBuilder
                .from((Activity) context)
                .setType("text/plain")
                .setChooserTitle("Share via")
                .setText(shareText)
                .startChooser();
    }

    public static void shareFact(final Context context, final String description) {
        String shareText = "Did you know?" + "\n" + description + "\n" + "\n" + "Download FOTD from Play Store:"
                + "\n" + "https://play.google.com/store/apps/details?id=com.indemand.fotd"
                + "\n" + "\n" + "Download FOTD from App Store:"
                + "\n" + "https://itunes.apple.com/az/app/fact-of-the-day-new-facts/id1450758752?mt=8";

        ShareCompat.IntentBuilder
                .from((Activity) context)
                .setType("text/plain")
                .setChooserTitle("Share via")
                .setText(shareText)
                .startChooser();
    }

    public static void openPlayStore(final Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + context.getPackageName())));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static String getAboutUsPage() {
        AppVersionDtls appVersionDtls = Dependencies.getAppData();
        if (appVersionDtls != null && appVersionDtls.getAboutUsPage() != null) {
            return appVersionDtls.getAboutUsPage();
        } else {
            return "file:///android_asset/about_us.html";
        }
    }

    public static void openInstagram(final Context context) {
        AppVersionDtls appVersionDtls = Dependencies.getAppData();
        String instaHandle = "fact__of_the__day";

        if (appVersionDtls != null && appVersionDtls.getInstaHandle() != null) {
            instaHandle = appVersionDtls.getInstaHandle();
        }
        String url = "https://www.instagram.com/" + instaHandle + "/";
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            if (context.getPackageManager().getPackageInfo("com.instagram.android", 0) != null) {
                intent.setData(Uri.parse("http://instagram.com/_u/" + instaHandle));
                intent.setPackage("com.instagram.android");
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    /**
     * Adds automation to the editText moving to next or previous
     *
     * @param mSize    size after which you want to move to next editText
     * @param editText reference to all the edit to move next or previous location
     */
    public static void moveFrontAndBack(final int mSize, final EditText... editText) {
        for (int pos = 0; pos < (editText.length - 1); pos++) {
            moveToNextEt(editText[pos], editText[pos + 1], mSize);
        }
        for (int pos = 1; pos < editText.length; pos++) {
            moveToPreviousEt(editText[pos], editText[pos - 1]);
        }
    }

    /**
     * adds Functionality to move automatically to next field or edit text
     *
     * @param mCurrentEt current reference to EditText
     * @param mNextEt    refernce to next editText shifted to
     * @param mSize      size after which to move to the next position
     */
    public static void moveToNextEt(final EditText mCurrentEt, final EditText mNextEt, final int mSize) {
        mCurrentEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if (mCurrentEt.getText().toString().length() == mSize) {
                    mNextEt.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(final Editable s) {

            }
        });
    }


    /**
     * adds Functionality to move automaticaly to previous field or edit text
     *
     * @param mCurrentEt current reference to EditText
     * @param mPrevEt    reference to next edit text shifted to
     */
    public static void moveToPreviousEt(final EditText mCurrentEt, final EditText mPrevEt) {
        mCurrentEt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View view, final int keyCode, final KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_DEL
                        && keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        && mCurrentEt.getText().toString().isEmpty()) {
                    mPrevEt.requestFocus();
                }
                return false;
            }
        });
    }

    public static boolean isFirstClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) { // 1500 = 1.5 second
            mLastClickTime = SystemClock.elapsedRealtime();
            return false;
        } else {
            mLastClickTime = SystemClock.elapsedRealtime();
            return true;
        }
    }

    /**
     * get date time in required format from ISO format
     *
     * @param inputDate     the input date
     * @param outputPattern the output pattern
     * @param isUTCTime     true if UTC time required
     * @return the return
     */
    public static String changeDateFromISOFormat(final String inputDate, final String outputPattern, final boolean isUTCTime) {
        SimpleDateFormat isoFormat = new SimpleDateFormat(ISO_FORMAT, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        Date selecteddate = null;
        try {
            selecteddate = isoFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long utcMill = 0;
        if (selecteddate != null) {
            utcMill = selecteddate.getTime();
            if (isUTCTime) {
                outputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            }
        }
        return outputFormat.format(new Date(utcMill));
    }

    public static String getLastUpdated(final String inputDate) {
        if (inputDate == null || inputDate.isEmpty()) {
            return "";
        }
        String updatedWhen = "";

        long longAddedWhenMilli = convertISODateToMillis(inputDate);
        long longCurrentTimeMilli = System.currentTimeMillis();

        long longAddedWhenMins = TimeUnit.MILLISECONDS.toMinutes(longAddedWhenMilli);
        long longCurrentTimeMins = TimeUnit.MILLISECONDS.toMinutes(longCurrentTimeMilli);

        long longUpdatedWhenInMins = longCurrentTimeMins - longAddedWhenMins;

        //just now for 0 or 1 min
        if (longUpdatedWhenInMins == 0 || longUpdatedWhenInMins == 1) {
            updatedWhen = "posted just now";
        }
        // in minutes for time in between 0-60 minutes
        else if (longUpdatedWhenInMins > 1 && longUpdatedWhenInMins <= 60) {
            updatedWhen = "posted " + longUpdatedWhenInMins + " minutes ago";
        }
        // in hours for time in between 60 min to 24*60 minutes
        else if (longUpdatedWhenInMins > 60 && longUpdatedWhenInMins <= 1440) {
            long longHours = longUpdatedWhenInMins / 60;
            updatedWhen = "posted " + longHours + " hours ago";
        }
        // in days for time between 24 hours and 5 days
        else if (longUpdatedWhenInMins > 1440 && longUpdatedWhenInMins <= 7200) {
            long longDays = longUpdatedWhenInMins / 1440;
            updatedWhen = "posted " + longDays + " days ago";
        }
        // in days greater than 5 days
        else {
            updatedWhen = "posted on " + changeDateFromISOFormat(inputDate, UI_FORMAT_DATE, true);
        }

        return updatedWhen;
    }

    private static long convertISODateToMillis(final String inputDate) {
        try {
            SimpleDateFormat isoFormat = new SimpleDateFormat(ISO_FORMAT);
            if (true) {
                isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            }
            return isoFormat.parse(inputDate).getTime();
            //return isoFormat.parse(inputDate).getTime() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
