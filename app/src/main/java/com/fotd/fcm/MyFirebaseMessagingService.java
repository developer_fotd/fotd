package com.fotd.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.fotd.ui.splash.SplashActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.indemand.fotd.R;

import java.util.Map;

/**
 * Developer: Click Labs
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getName();
    public static FCMTokenInterface fcmTokenCallback;
    private static Handler handlerOs = new Handler();
    private static final int FCM_CALL_TIMEOUT = 20000;
    private NotificationManager notificationManager;
    //private final long[] NOTIFICATION_VIBRATION_PATTERN = new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400};
    private final String DEFAULT_CHANNEL_ID = "default";

    /**
     * Sets callback.
     *
     * @param callback the callback
     */
    public static void setCallback(final FCMTokenInterface callback) {
        fcmTokenCallback = callback;
        startHandler();
        initFCM();
    }

    private static void initFCM() {
        /*FirebaseMessaging.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(final InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                if (fcmTokenCallback != null) {
                    fcmTokenCallback.onTokenReceived(token);
                    fcmTokenCallback = null;
                    clearHandler();
                }
            }
        });*/

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                //if(task.isComplete()){
                if (!task.isSuccessful()) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                    return;
                }

                String token = task.getResult();
                if (fcmTokenCallback != null) {
                    fcmTokenCallback.onTokenReceived(token);
                    fcmTokenCallback = null;
                    clearHandler();
                }

                //}
            }
        });
    }

    private static void startHandler() {
        handlerOs.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fcmTokenCallback != null) {
                    fcmTokenCallback.onFailure();
                    fcmTokenCallback = null;
                }
            }
        }, FCM_CALL_TIMEOUT);
    }

    @Override
    public void onNewToken(@NonNull final String token) {
        super.onNewToken(token);
        if (fcmTokenCallback != null) {
            fcmTokenCallback.onTokenReceived(token);
            fcmTokenCallback = null;
            clearHandler();
        }
    }

    /**
     *
     */
    private static void clearHandler() {
        handlerOs.removeCallbacksAndMessages(null);
    }

    /**
     * Retry.
     *
     * @param callback the callback
     */
    public static void retry(final FCMTokenInterface callback) {
        setCallback(callback);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        Log.d(TAG, getString(R.string.log_fcm_message_id) + remoteMessage.getMessageId());
        //   Log.d(TAG, "FCM Notification Body: " + remoteMessage.getNotification().getBody());
        Log.d(TAG, getString(R.string.log_fcm_notification_message) + remoteMessage.getNotification());
        Log.d(TAG, getString(R.string.log_fcm_data) + remoteMessage.getData());
        showNotification(remoteMessage.getData());
    }

    /**
     * @param data notification data
     */
    public void showNotification(final Map<String, String> data) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(getApplicationContext(), SplashActivity.class);

        PendingIntent pi = PendingIntent.getActivity(this,
                    0, notificationIntent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S ?
                            PendingIntent.FLAG_IMMUTABLE : PendingIntent.FLAG_UPDATE_CURRENT);

        String sound = "android.resource://" + getPackageName() + "/" + R.raw.eventually;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Creating an Audio Attribute
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;

            // Default channel
            NotificationChannel mChannel = new NotificationChannel(DEFAULT_CHANNEL_ID, "Notifications",
                    importance);
            // Configure the notification channel.
            mChannel.setDescription("Latest Updates");
            mChannel.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mChannel.setLightColor(Color.RED);
            //mChannel.enableVibration(true);
            //mChannel.setVibrationPattern(NOTIFICATION_VIBRATION_PATTERN);
            //mChannel.setSound(Uri.parse(sound), audioAttributes);
            notificationManager.createNotificationChannel(mChannel);
        }

        //Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this, DEFAULT_CHANNEL_ID)
                //     .setTicker(r.getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(data.get("message")))
                .setSmallIcon(R.mipmap.ic_notif)
                //.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(data.get("title"))
                .setContentText(data.get("message"))
                .setContentIntent(pi)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true);

        //mNotificationBuilder.setSound(Uri.parse(sound));
        mNotificationBuilder.setChannelId(DEFAULT_CHANNEL_ID);
        notificationManager.notify(0, mNotificationBuilder.build());
    }
}
