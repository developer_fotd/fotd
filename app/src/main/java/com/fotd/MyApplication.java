package com.fotd;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.fotd.util.Foreground;
import com.indemand.fotd.BuildConfig;
import com.indemand.fotd.R;

import java.lang.ref.WeakReference;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;


/**
 * Developer: Click Labs
 */
public class MyApplication extends Application {

    private static WeakReference<Context> mWeakReference;
    private static SharedPreferences.Editor INSTANCE_1;
    private static SharedPreferences INSTANCE_2;
    private static Activity mActivity;

    /**
     * Getter to access Singleton instance
     *
     * @return instance of MyApplication
     */
    public static Context getAppContext() {
        return mWeakReference.get();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        if (!BuildConfig.DEBUG) {
            FirebaseAnalytics.getInstance(this);
        }
        Foreground.init(this);
        mWeakReference = new WeakReference<Context>(this);

        // initialize the AdMob app
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Gill-Sans-Medium.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build())).build());
    }

    /**
     * get reference of shared preference to edit
     *
     * @return the editor
     */
    public static SharedPreferences.Editor getSharedPrefEditorRef(final Context context) {
        if (INSTANCE_2 == null) {
            INSTANCE_2 = context.getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        }

        if (INSTANCE_1 == null) {
            INSTANCE_1 = INSTANCE_2.edit();
        }

        return INSTANCE_1;
    }

    /**
     * get reference of shared preference
     *
     * @return the shared preferences
     */
    public static SharedPreferences getSharedPrefRef(final Context context) {
        if (INSTANCE_2 == null) {
            INSTANCE_2 = context.getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        }
        return INSTANCE_2;
    }

    public static void setCurrentRunningActivity(final Activity activity) {
        mActivity = activity;
    }

    public static Activity getCurrentRunningActivity() {
        return mActivity;
    }

}
