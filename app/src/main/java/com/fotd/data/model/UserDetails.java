package com.fotd.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Developer: Click Labs
 */
public class UserDetails implements Parcelable {

    @SerializedName("user_id")
    private int userId;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("userFactCount")
    private FactCountDetails userFactCount;

    @SerializedName("profile_image")
    private String profileImage;

    @SerializedName("notification_enabled")
    private int notificationEnabled;

    @SerializedName("avatars")
    private ArrayList<String> avatars;

    protected UserDetails(Parcel in) {
        userId = in.readInt();
        name = in.readString();
        email = in.readString();
        accessToken = in.readString();
        userFactCount = in.readParcelable(FactCountDetails.class.getClassLoader());
        profileImage = in.readString();
        notificationEnabled = in.readInt();
        avatars = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(accessToken);
        dest.writeParcelable(userFactCount, flags);
        dest.writeString(profileImage);
        dest.writeInt(notificationEnabled);
        dest.writeStringList(avatars);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDetails> CREATOR = new Creator<UserDetails>() {
        @Override
        public UserDetails createFromParcel(Parcel in) {
            return new UserDetails(in);
        }

        @Override
        public UserDetails[] newArray(int size) {
            return new UserDetails[size];
        }
    };

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public FactCountDetails getUserFactCount() {
        return userFactCount;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(final String profileImage) {
        this.profileImage = profileImage;
    }

    public int getNotificationEnabled() {
        return notificationEnabled;
    }

    public void setNotificationEnabled(final int notificationEnabled) {
        this.notificationEnabled = notificationEnabled;
    }

    public ArrayList<String> getAvatars() {
        return avatars;
    }
}
