package com.fotd.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class FactCountDetails implements Parcelable {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("pending_count")
    private int pendingCount;

    @SerializedName("approved_count")
    private int approvedCount;

    @SerializedName("rejected_count")
    private int rejectedCount;

    protected FactCountDetails(Parcel in) {
        totalCount = in.readInt();
        pendingCount = in.readInt();
        approvedCount = in.readInt();
        rejectedCount = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(totalCount);
        dest.writeInt(pendingCount);
        dest.writeInt(approvedCount);
        dest.writeInt(rejectedCount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FactCountDetails> CREATOR = new Creator<FactCountDetails>() {
        @Override
        public FactCountDetails createFromParcel(Parcel in) {
            return new FactCountDetails(in);
        }

        @Override
        public FactCountDetails[] newArray(int size) {
            return new FactCountDetails[size];
        }
    };

    public int getTotalCount() {
        return totalCount;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public int getApprovedCount() {
        return approvedCount;
    }

    public int getRejectedCount() {
        return rejectedCount;
    }
}
