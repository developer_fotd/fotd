package com.fotd.data.model;

/**
 * Developer: Click Labs
 */
public class CommentObj {

    private String comment;
    private boolean isSelected;

    public CommentObj(final String comment) {
        this.comment = comment;
        this.isSelected = false;
    }

    public String getComment() {
        return comment;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(final boolean selected) {
        isSelected = selected;
    }
}
