package com.fotd.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class UserAPIResponse {

    @SerializedName("userInfo")
    private UserDetails userDetails;

    public UserDetails getUserDetails() {
        return userDetails;
    }
}
