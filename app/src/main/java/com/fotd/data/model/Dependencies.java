package com.fotd.data.model;

import android.content.Context;

import com.google.gson.Gson;
import com.fotd.MyApplication;

/**
 * Developer: Click Labs
 */
public class Dependencies {

    private static final String SAVED_FCM_TOKEN = "saved_fcm_token";
    private static final String SAVE_IS_GUEST = "save_is_guest";
    private static final String RATING_SKIP_COUNTER = "saved_facts_new";
    private static final String PREF_USER_DETAILS = "pref_user_details";
    private static final String PREF_APP_DATA = "pref_app_data";
    private static final String PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN";
    private static final String PREF_IS_RATED = "pref_is_rated";

    public static String getString(final String key) {
        return MyApplication.getSharedPrefRef(MyApplication.getAppContext()).getString(key, "");
    }

    public static void saveFCMToken(final String fcmToken) {
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).putString(SAVED_FCM_TOKEN, fcmToken).commit();
    }

    public static String getFCMToken() {
        return MyApplication.getSharedPrefRef(MyApplication.getAppContext()).getString(SAVED_FCM_TOKEN, "");
    }

    public static void saveGuestDtls(final boolean isGuestUser) {
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).putBoolean(SAVE_IS_GUEST, isGuestUser).commit();
    }

    public static boolean isGuest() {
        return MyApplication.getSharedPrefRef(MyApplication.getAppContext()).getBoolean(SAVE_IS_GUEST, false);
    }

    public static UserDetails getUserDetails() {
        return new Gson().fromJson(Dependencies.getString(PREF_USER_DETAILS), UserDetails.class);
    }

    public static void saveUserDetails(final UserDetails userDetails) {
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).putString(PREF_USER_DETAILS,
                new Gson().toJson(userDetails)).commit();
    }

    public static AppVersionDtls getAppData() {
        return new Gson().fromJson(Dependencies.getString(PREF_APP_DATA), AppVersionDtls.class);
    }

    public static void saveAppData(final AppVersionDtls appVersionDtls) {
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).putString(PREF_APP_DATA,
                new Gson().toJson(appVersionDtls)).commit();
    }

    public static void saveAccessToken(final String accessToken) {
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).putString(PREF_ACCESS_TOKEN, accessToken).commit();
    }

    public static String getAccessToken() {
        return MyApplication.getSharedPrefRef(MyApplication.getAppContext()).getString(PREF_ACCESS_TOKEN, "");
    }

    public static void increaseRatingCounter(final Context context) {
        int skipCount = MyApplication.getSharedPrefRef(context)
                .getInt(RATING_SKIP_COUNTER, 0);
        MyApplication.getSharedPrefEditorRef(context).putInt(RATING_SKIP_COUNTER, ++skipCount).commit();
    }

    public static void clearRatingCounter(final Context context) {
        MyApplication.getSharedPrefEditorRef(context).putInt(RATING_SKIP_COUNTER, 0).commit();
    }

    public static int getRatingCounter(final Context context) {
        return MyApplication.getSharedPrefRef(context)
                .getInt(RATING_SKIP_COUNTER, 0);
    }

    public static void saveAsRated(final Context context, final boolean isUserRated) {
        clearRatingCounter(context);
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).putBoolean(PREF_IS_RATED, isUserRated).commit();
    }

    public static boolean isRated(final Context context) {
        return MyApplication.getSharedPrefRef(context).getBoolean(PREF_IS_RATED, false);
    }

    public static void clearData() {
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).remove(PREF_ACCESS_TOKEN).commit();
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).remove(SAVE_IS_GUEST).commit();
        MyApplication.getSharedPrefEditorRef(MyApplication.getAppContext()).remove(PREF_IS_RATED).commit();
    }
}
