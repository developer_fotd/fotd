package com.fotd.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class TodaysAPIResponse {

    @SerializedName("fact")
    private FactDetails factDetails;

    @SerializedName("like_count")
    private int likeCount;

    @SerializedName("dislike_count")
    private int dislikeCount;

    @SerializedName("user_like_status")
    private int userLikeStatus;

    @SerializedName("user_fav_status")
    private int userFavStatus;

    public FactDetails getFactDetails() {
        return factDetails;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public int getUserLikeStatus() {
        return userLikeStatus;
    }

    public int getUserFavStatus() {
        return userFavStatus;
    }

    public void setLikeDislikeResponse(final int userLikeStatus, final int likeCount, final int dislikeCount) {
        this.userLikeStatus = userLikeStatus;
        this.likeCount = likeCount;
        this.dislikeCount = dislikeCount;
    }

    public void setUserFavStatus(final int userFavStatus) {
        this.userFavStatus = userFavStatus;
    }
}
