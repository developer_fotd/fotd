package com.fotd.data.model;

/**
 * Developer: Click Labs
 */
public class AvatarObj {

    private String avatarURL;
    private boolean isSelected;

    public AvatarObj(final String avatarURL,boolean isSelected) {
        this.avatarURL = avatarURL;
        this.isSelected = isSelected;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(final boolean selected) {
        isSelected = selected;
    }
}
