package com.fotd.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class OTPAPIResponse {

    @SerializedName("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }
}
