package com.fotd.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Developer: Click Labs
 */
public class HomeResponse {

    @SerializedName("featured")
    private ArrayList<FactDetails> featured;

    @SerializedName("popular")
    private ArrayList<FactDetails> popular;

    public ArrayList<FactDetails> getFeatured() {
        return featured;
    }

    public ArrayList<FactDetails> getPopular() {
        return popular;
    }
}
