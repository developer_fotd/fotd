package com.fotd.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class AppVersionDtls implements Parcelable {

    @SerializedName("is_force_update")
    private boolean isForceUpdate;

    @SerializedName("is_manual_update")
    private boolean isManualUpdate;

    @SerializedName("app_link")
    private String link;

    @SerializedName("package_name")
    private String packageName;

    @SerializedName("about_us_page")
    private String aboutUsPage;

    @SerializedName("insta_handle")
    private String instaHandle;

    @SerializedName("fb_handle")
    private String fbHandle;

    @SerializedName("ad_mob_enabled")
    private int adMobEnabled;

    protected AppVersionDtls(Parcel in) {
        isForceUpdate = in.readByte() != 0;
        isManualUpdate = in.readByte() != 0;
        link = in.readString();
        packageName = in.readString();
        aboutUsPage = in.readString();
        instaHandle = in.readString();
        fbHandle = in.readString();
        adMobEnabled = in.readInt();
    }

    public static final Creator<AppVersionDtls> CREATOR = new Creator<AppVersionDtls>() {
        @Override
        public AppVersionDtls createFromParcel(Parcel in) {
            return new AppVersionDtls(in);
        }

        @Override
        public AppVersionDtls[] newArray(int size) {
            return new AppVersionDtls[size];
        }
    };

    public boolean isForceUpdate() {
        return isForceUpdate;
    }

    public boolean isManualUpdate() {
        return isManualUpdate;
    }

    public String getLink() {
        return link;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getAboutUsPage() {
        return aboutUsPage;
    }

    public String getInstaHandle() {
        return instaHandle;
    }

    public String getFbHandle() {
        return fbHandle;
    }

    public int getAdMobEnabled() {
        return adMobEnabled;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeByte((byte) (isForceUpdate ? 1 : 0));
        dest.writeByte((byte) (isManualUpdate ? 1 : 0));
        dest.writeString(link);
        dest.writeString(packageName);
        dest.writeString(aboutUsPage);
        dest.writeString(instaHandle);
        dest.writeString(fbHandle);
        dest.writeInt(adMobEnabled);
    }
}
