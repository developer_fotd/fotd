package com.fotd.data.model;

import java.util.ArrayList;

/**
 * Developer: Click Labs
 */
public class FactResponse {

    private ArrayList<FactDetails> facts;

    public ArrayList<FactDetails> getFacts() {
        return facts;
    }
}
