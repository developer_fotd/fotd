package com.fotd.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class LikeAPIResponse {

    @SerializedName("like_count")
    private int likeCount;

    @SerializedName("dislike_count")
    private int dislikeCount;

    public int getLikeCount() {
        return likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }
}
