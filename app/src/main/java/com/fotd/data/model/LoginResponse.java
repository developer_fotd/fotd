package com.fotd.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class LoginResponse {

    @SerializedName("status")
    private boolean success;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private UserDetails userDetails;

    public boolean isSuccess() {
        return success;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public String getMessage() {
        return message;
    }
}
