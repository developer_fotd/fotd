package com.fotd.data.model;

import com.indemand.fotd.R
import com.fotd.util.AppConstants

/**
 * Developer: Click Labs
 */
open class ColorCodes {

    var bgColor: Int = 0
    var textColor: Int = 0
    var likeDefaultResId = 0
    var likePressedResId = 0
    var dislikeDefaultResId = 0
    var dislikePressedResId = 0
    var favDefaultResId = 0
    var favPressedResId = 0
    var shareResId = 0

    constructor(bgColor: Int, textColor: Int, likeDefaultResId: Int, likePressedResId: Int, dislikeDefaultResId: Int,
                dislikePressedResId: Int, favDefaultResId: Int, favPressedResId: Int, shareResId: Int) {
        this.bgColor = bgColor
        this.textColor = textColor
        this.likeDefaultResId = likeDefaultResId
        this.likePressedResId = likePressedResId
        this.dislikeDefaultResId = dislikeDefaultResId
        this.dislikePressedResId = dislikePressedResId
        this.favDefaultResId = favDefaultResId
        this.favPressedResId = favPressedResId
        this.shareResId = shareResId
    }


    companion object {
        var bgColor: Int = 0
        var textColor: Int = 0
        var likeDefaultResId = 0
        var likePressedResId = 0
        var dislikeDefaultResId = 0
        var dislikePressedResId = 0
        var favDefaultResId = 0
        var favPressedResId = R.drawable.ic_fav_pressed
        var shareResId = 0

        fun getColor(position: Int): ColorCodes {
            if (AppConstants.COLOR_RED.contains(position)) {
                bgColor = R.color.fact_bg_0
                textColor = R.color.white_default

                likeDefaultResId = R.drawable.ic_like_default_white
                likePressedResId = R.drawable.ic_like_pressed_white
                dislikeDefaultResId = R.drawable.ic_dislike_default_white
                dislikePressedResId = R.drawable.ic_dislike_pressed_white

                favDefaultResId = R.drawable.ic_fav_default_white
                shareResId = R.drawable.ic_share_white
            } else if (AppConstants.COLOR_L_BLUE.contains(position)) {
                bgColor = R.color.fact_bg_1
                textColor = R.color.black_default

                likeDefaultResId = R.drawable.ic_like_default_black
                likePressedResId = R.drawable.ic_like_pressed_black
                dislikeDefaultResId = R.drawable.ic_dislike_default_black
                dislikePressedResId = R.drawable.ic_dislike_pressed_black

                favDefaultResId = R.drawable.ic_fav_default_black
                shareResId = R.drawable.ic_share_black
            } else if (AppConstants.COLOR_D_GREEN.contains(position)) {
                bgColor = R.color.fact_bg_2
                textColor = R.color.white_default

                likeDefaultResId = R.drawable.ic_like_default_white
                likePressedResId = R.drawable.ic_like_pressed_white
                dislikeDefaultResId = R.drawable.ic_dislike_default_white
                dislikePressedResId = R.drawable.ic_dislike_pressed_white

                favDefaultResId = R.drawable.ic_fav_default_white
                shareResId = R.drawable.ic_share_white
            } else if (AppConstants.COLOR_D_BLUE.contains(position)) {
                bgColor = R.color.fact_bg_3
                textColor = R.color.white_default

                likeDefaultResId = R.drawable.ic_like_default_white
                likePressedResId = R.drawable.ic_like_pressed_white
                dislikeDefaultResId = R.drawable.ic_dislike_default_white
                dislikePressedResId = R.drawable.ic_dislike_pressed_white

                favDefaultResId = R.drawable.ic_fav_default_white
                shareResId = R.drawable.ic_share_white
            } else if (AppConstants.COLOR_YELLOW.contains(position)) {
                bgColor = R.color.fact_bg_4
                textColor = R.color.black_default

                likeDefaultResId = R.drawable.ic_like_default_black
                likePressedResId = R.drawable.ic_like_pressed_black
                dislikeDefaultResId = R.drawable.ic_dislike_default_black
                dislikePressedResId = R.drawable.ic_dislike_pressed_black

                favDefaultResId = R.drawable.ic_fav_default_black
                shareResId = R.drawable.ic_share_black
            } else if (AppConstants.COLOR_L_GREEN.contains(position)) {
                bgColor = R.color.fact_bg_5
                textColor = R.color.black_default

                likeDefaultResId = R.drawable.ic_like_default_black
                likePressedResId = R.drawable.ic_like_pressed_black
                dislikeDefaultResId = R.drawable.ic_dislike_default_black
                dislikePressedResId = R.drawable.ic_dislike_pressed_black

                favDefaultResId = R.drawable.ic_fav_default_black
                shareResId = R.drawable.ic_share_black
            } else {
                bgColor = R.color.fact_bg_0
                textColor = R.color.white_default

                likeDefaultResId = R.drawable.ic_like_default_white
                likePressedResId = R.drawable.ic_like_pressed_white
                dislikeDefaultResId = R.drawable.ic_dislike_default_white
                dislikePressedResId = R.drawable.ic_dislike_pressed_white

                favDefaultResId = R.drawable.ic_fav_default_white
                shareResId = R.drawable.ic_share_white
            }

            return ColorCodes(bgColor, textColor, likeDefaultResId, likePressedResId, dislikeDefaultResId, dislikePressedResId,
                    favDefaultResId, favPressedResId, shareResId)
        }
    }
}
