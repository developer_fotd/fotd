package com.fotd.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Developer: Click Labs
 */
public class FactDetails implements Parcelable {

    @SerializedName("fact_id")
    private int factId;

    @SerializedName("like_count")
    private int likeCount;

    @SerializedName("dislike_count")
    private int dislikeCount;

    @SerializedName("user_like_status")
    private int userLikeStatus;

    @SerializedName("user_fav_status")
    private int userFavStatus;

    @SerializedName("added_by")
    private String addedBy;

    @SerializedName("added_on")
    private String addedOn;

    @SerializedName("fact")
    private String fact;

    @SerializedName("user_image")
    private String userImage;

    protected FactDetails(Parcel in) {
        factId = in.readInt();
        likeCount = in.readInt();
        dislikeCount = in.readInt();
        userLikeStatus = in.readInt();
        userFavStatus = in.readInt();
        addedBy = in.readString();
        addedOn = in.readString();
        fact = in.readString();
        userImage = in.readString();
    }

    public static final Creator<FactDetails> CREATOR = new Creator<FactDetails>() {
        @Override
        public FactDetails createFromParcel(Parcel in) {
            return new FactDetails(in);
        }

        @Override
        public FactDetails[] newArray(int size) {
            return new FactDetails[size];
        }
    };

    public int getFactId() {
        return factId;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public int getUserLikeStatus() {
        return userLikeStatus;
    }

    public int getUserFavStatus() {
        return userFavStatus;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public String getFact() {
        return fact;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setLikeCount(final int likeCount) {
        this.likeCount = likeCount;
    }

    public void setDislikeCount(final int dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public void setUserLikeStatus(final int userLikeStatus) {
        this.userLikeStatus = userLikeStatus;
    }

    public void setLikeDislikeResponse(final int userLikeStatus, final int likeCount, final int dislikeCount) {
        this.userLikeStatus = userLikeStatus;
        this.likeCount = likeCount;
        this.dislikeCount = dislikeCount;
    }

    public void setUserFavStatus(final int userFavStatus) {
        this.userFavStatus = userFavStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(factId);
        dest.writeInt(likeCount);
        dest.writeInt(dislikeCount);
        dest.writeInt(userLikeStatus);
        dest.writeInt(userFavStatus);
        dest.writeString(addedBy);
        dest.writeString(addedOn);
        dest.writeString(fact);
        dest.writeString(userImage);
    }
}
