package com.clicklabs.data.network

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

@JvmSuppressWildcards
interface ApiInterface {
    @GET
    fun getData(@Url url: String, @QueryMap map: Map<String, Any>): Call<CommonResponse>

    @FormUrlEncoded
    @POST
    fun postData(@Url url: String, @FieldMap map: Map<String, Any>): Call<CommonResponse>

    @Multipart
    @POST
    fun postMultipartData(@Url url: String, @PartMap map: Map<String, RequestBody>): Call<CommonResponse>
}