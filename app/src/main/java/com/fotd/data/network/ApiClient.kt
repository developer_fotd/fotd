package com.clicklabs.data.network

import com.indemand.fotd.BuildConfig
import com.indemand.fotd.BuildConfig.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiClient {
    companion object {
        private val TIME_OUT = 120;
        private var retrofit: Retrofit? = null

        fun getClient(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient().build())
                        .build()
            }
            return retrofit
        }

        private fun httpClient(): OkHttpClient.Builder {
            val httpClient = OkHttpClient.Builder()
            httpClient.readTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            httpClient.connectTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)

            // add headers
            //httpClient.addInterceptor(getHeadersInterceptor());

            // add logging as last interceptor
            httpClient.addInterceptor(getLoggingInterceptor());

            return httpClient
        }

        private fun getLoggingInterceptor(): HttpLoggingInterceptor {
            val loggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            }
            return loggingInterceptor;
        }

        /*private fun getHeadersInterceptor(): Interceptor {
            return Interceptor { chain ->
                val builder: Request.Builder = chain.request().newBuilder()
                builder.addHeader(
                    HEADER_APP_VERSION, Integer.toString(BuildConfig.VERSION_CODE)
                )
                builder.addHeader(
                    HEADER_OFFSET, Helper.getCurrentZoneOffset()
                )
                builder.addHeader(HEADER_LANGUAGE, "en")
                chain.proceed(builder.build())
            }
        }*/
    }
}