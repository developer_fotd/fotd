package com.fotd.data.network;

/**
 * Created by Mohit @KR Solutions © 2019. ALL RIGHTS RESERVED.
 */
public interface APIKeyConstant {

    String API_APP_VERSION = "app_version";
    String API_ACCESS_TOKEN = "access_token";
    String API_DEVICE_TOKEN = "device_token";
    String API_DEVICE_TYPE = "device_type";
    String API_DEVICE_NAME = "device_name";
    String API_PASSWORD = "password";
    String API_OLD_PASSWORD = "old_password";
    String API_NEW_PASSWORD = "new_password";
    String API_NAME = "name";
    String API_EMAIL = "email";
    String API_OTP = "otp";
    String API_FACT_ID = "fact_id";
    String API_STATUS = "status";
    String API_SEARCH_STRING = "search_string";
    String API_FACT_TYPE = "fact_type";
    String API_FACT_STATUS = "fact_status";
    String API_LIMIT = "limit";
    String API_SKIP = "skip";
    String API_FACT = "fact";
    String API_RATING = "rating";
    String API_FEEDBACK = "feedback";
    String API_COMMENTS = "comments";
    String API_NOTIFICATION_ENABLED = "notification_enabled";
    String API_IMAGE = "file";
    String API_TIMEZONE = "timezone";
    String API_TIMEZONE_INFO = "timezone_info";
    String API_USER_FAV_FACTS = "need_user_fav_facts";
    String API_AVATAR = "profile_image";
}
