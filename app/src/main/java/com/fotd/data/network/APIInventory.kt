package com.clicklabs.data.network

class APIInventory {
    companion object {
        const val CHECK_APP_VERSION = "app/version"
        const val CHECK_ACCESS_TOKEN = "user/loginViaAccessToken"
        const val LOGIN_USER = "user/login"
        const val SIGNUP_USER = "user/register"
        const val SEND_PASSWORD_LINK = "user/forgetPassword"
        const val VERIFY_OTP = "user/verifyOtp"
        const val EDIT_PROFILE = "user/editProfile"
        const val GET_BLOG_DETAILS = "fact/featured"
        const val LOGOUT_USER = "user/logout"
        const val GET_TODAYS_FACT = "fact/today"
        const val FACT_LIKE = "fact/like"
        const val FACT_FAVOURITE = "fact/favourite/add"
        const val GET_MY_FACTS = "fact/userAddedfact"
        const val GET_FACTS = "fact/v2/get"
        const val ADD_FACT = "fact/add"
        const val ADD_FEEDBACK = "feedback/add"
        const val CHANGE_PASSWORD = "user/changePassword"
        const val GET_FACT_DETAILS = "fact/getDetails"
    }
}