package com.clicklabs.data.network

import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CommonResponse {

    @SerializedName("status")
    @Expose
    val statusCode = 0
        get() = field

    @SerializedName("message")
    @Expose
    val message: String? = null
        get() = field

    @SerializedName("data")
    @Expose
    private val data: Any? = null


    fun <T> toResponseModel(classRef: Class<T>?): T {
        return Gson().fromJson(Gson().toJson(data), classRef)
    }
}