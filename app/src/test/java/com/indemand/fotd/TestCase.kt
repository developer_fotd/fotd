package com.indemand.fotd

import android.content.Context
import com.fotd.ui.login.LoginActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TestCase {

    @Mock
    lateinit var context: Context
    lateinit var loginActivity: LoginActivity

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        loginActivity = LoginActivity()
    }

    @Test
    fun checkValidations() {
        loginActivity.mPresenter.loginUser("", "")
    }

}