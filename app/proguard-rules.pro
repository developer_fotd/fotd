# Retrofit 2.X
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

-dontwarn okio.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
 #paper Db
 #-keep class com.skeleton.model.** { *; }

-keep class org.objenesis.** { *; }
-keep interface org.objenesis.** { *; }
-dontwarn org.objenesis.**

-keep public class com.fotd.data.model.** {
  public protected private *;
}

-keep class com.squareup.picasso.**
-dontwarn com.squareup.picasso.**
-dontwarn com.squareup.okhttp.**

-keepclassmembers class **.R$* {
    public static <fields>;
}

-dontwarn android.support.**
-dontwarn com.fasterxml.jackson.**
-dontwarn org.simpleframework.**
-dontwarn org.springframework.http.converter.json.**
-dontwarn org.springframework.http.converter.feed.**
-dontwarn org.springframework.http.client.**
-dontwarn java.beans.**
-dontwarn org.springframework.core.convert.support.**

-keep class go.** { *; }


# -keep class cl.ionix.mobile.checkout.**
# -dontwarn cl.ionix.mobile.checkout.**
# -dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry
#
-dontwarn okhttp3.**
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Generell
-keepattributes SourceFile,LineNumberTable,*Annotation*,EnclosingMethod,Signature,Exceptions,InnerClasses

-keep class com.wang.avi.** { *; }
-keep class com.wang.avi.indicators.** { *; }